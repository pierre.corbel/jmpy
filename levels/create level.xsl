<?xml version="1.0"?>
<xsl:stylesheet xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">

<xsl:template match="//*[local-name()='rect']">
<xsl:variable name="translatecouple" select="replace(./@*[local-name()='transform'], 'translate\(','')" />
<xsl:variable name="translatecouple" select="replace($translatecouple, '\)','')" />
<xsl:variable name="translateX">
<xsl:choose>
<xsl:when test=" $translatecouple='' ">0</xsl:when>
<xsl:otherwise><xsl:value-of select="replace($translatecouple, '.*,','')"/></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="translateY">
<xsl:choose>
<xsl:when test=" $translatecouple='' ">0</xsl:when>
<xsl:otherwise><xsl:value-of select="replace($translatecouple, '.*,','')"/></xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:choose>
   <xsl:when test="contains(./@style,'#000000')">hitBox</xsl:when>
   <xsl:when test="contains(./@style,'#808000')">wallJumpHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#00ff00')">flagHitbox</xsl:when>
   <xsl:when test="contains(./@style,'#ff0000')">dieHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#ffff00')">bounceHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#0000ff')">reverseGravity</xsl:when>
   <xsl:when test="contains(./@style,'#1a1a1a')">zeroGHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#008000')">//startPoint</xsl:when>
   
   
</xsl:choose>.addRect(new RectF(<xsl:value-of select="round(./@x+$translateX)" disable-output-escaping="yes" />, <xsl:value-of select="round(./@y+$translateY)" disable-output-escaping="yes" />, <xsl:value-of select="round(./@x+$translateX)+round(./@width)" disable-output-escaping="yes" />, <xsl:value-of select="round(./@y+$translateY)+round(./@height)" disable-output-escaping="yes" />), Direction.CCW);
</xsl:template>



<xsl:template match="//*[local-name()='path']">
<xsl:variable name="translatecouple" select="replace(./@*[local-name()='transform'], 'translate\(','')" />
<xsl:variable name="translatecouple" select="replace($translatecouple, '\)','')" />
<xsl:variable name="translateX">
	<xsl:choose>
		<xsl:when test=" $translatecouple='' ">0</xsl:when>
		<xsl:otherwise><xsl:value-of select="replace($translatecouple, ',.*','')"/></xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="translateY">
	<xsl:choose>
		<xsl:when test=" $translatecouple='' ">0</xsl:when>
		<xsl:otherwise><xsl:value-of select="replace($translatecouple, '.*,','')"/></xsl:otherwise>
	</xsl:choose>
</xsl:variable>
<xsl:choose>
   <xsl:when test="contains(./@style,'#ff0000')">dieHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#808000')">wallJumpHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#00ffff')">wallJumpHitBox	</xsl:when>
   <xsl:when test="contains(./@style,'ffff00')">bounceHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#0000ff')">reverseGravity</xsl:when>
   <xsl:when test="contains(./@style,'#1a1a1a')">zeroGHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#008000')">//startPoint</xsl:when>
   <xsl:when test="contains(./@style,'#00ff00')">flagHitbox</xsl:when>
</xsl:choose>.addCircle(<xsl:value-of select="round(./@*[local-name()='cx']+$translateX)" />,<xsl:value-of select="round(./@*[local-name()='cy']+$translateY)" />, <xsl:value-of select="round(./@*[local-name()='rx'])" />, Direction.CCW);
</xsl:template>

</xsl:stylesheet>
