package jmpy.levels.plateform.walljump2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;

public class PlateformWallJump2Land extends PlateformLand {

	

	public PlateformWallJump2Land(LevelView camera) {
		super(camera);

		  
		  
		  
		  
	    hitBox.addRect(new RectF(3, 3, 25, 1786), Direction.CCW);

	    hitBox.addRect(new RectF(-0, 0, 1350, 26), Direction.CCW);

	    hitBox.addRect(new RectF(-591, 734, -580, 810), Direction.CCW);

	    hitBox.addRect(new RectF(265, 311, 1067, 343), Direction.CCW);

	    hitBox.addRect(new RectF(1323, 6, 1352, 1789), Direction.CCW);

	    hitBox.addRect(new RectF(4, 1738, 1331, 1788), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-689, 998, -665, 1016), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-594, 1019, -567, 1039), Direction.CCW);

	    flagHitbox.addCircle(-609,1088, 15, Direction.CCW);

	    hitBox.addRect(new RectF(-924, 962, -914, 1111), Direction.CCW);

	    //startPoint.addCircle(-671,1072, 15, Direction.CCW);

	    dieHitBox.addCircle(-522,1136, 44, Direction.CCW);

	    hitBox.addRect(new RectF(877, 1602, 1055, 1752), Direction.CCW);

	    hitBox.addRect(new RectF(4, 1497, 705, 1531), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(15, 1206, 39, 1486), Direction.CCW);

	    hitBox.addRect(new RectF(243, 1258, 568, 1292), Direction.CCW);

	    hitBox.addRect(new RectF(1030, 500, 1071, 1313), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(1019, 736, 1040, 1313), Direction.CCW);

	    dieHitBox.addCircle(783,1194, 44, Direction.CCW);

	    hitBox.addRect(new RectF(754, 330, 795, 990), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(781, 405, 802, 996), Direction.CCW);

	    dieHitBox.addRect(new RectF(23, 1720, 1350, 1770), Direction.CCW);

	    hitBox.addRect(new RectF(1036, 442, 1335, 502), Direction.CCW);

	    dieHitBox.addCircle(1055,764, 44, Direction.CCW);

	    hitBox.addRect(new RectF(423, 667, 748, 701), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(15, 188, 36, 912), Direction.CCW);

	    flagHitbox.addCircle(716,648, 15, Direction.CCW);

	    //startPoint.addCircle(1009,1583, 15, Direction.CCW);

	    dieHitBox.addRect(new RectF(-647, 1004, -625, 1022), Direction.CCW);

	    hitBox.addRect(new RectF(-789, 427, -484, 447), Direction.CCW);
	    

	    getAnimatedLands().add(new DeathBlink(new RectF(303, 14, 308, 1271), camera, 100, 50, (float)0.5));
									
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(true, canvas);
	}

}
