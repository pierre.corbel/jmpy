package com.jmpy;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.layout.Home;
import com.jmpy.levels.cart._1.Cart1View;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.jetpack._1.JetPack1View;
import com.jmpy.levels.jetpack._2.JetPack2View;
import com.jmpy.levels.plateform._1.Plateform1View;
import com.jmpy.levels.plateform._2.Plateform2View;
import com.jmpy.levels.plateform._4.Plateform4View;
import com.jmpy.levels.plateform._5.Plateform5View;
import com.jmpy.levels.plateform.bounce0.PlateformBounce0View;
import com.jmpy.levels.plateform.bounce1.PlateformBounce1View;
import com.jmpy.levels.plateform.bounce4.PlateformBounce4View;
import com.jmpy.levels.plateform.death1.PlateformDeath1View;
import com.jmpy.levels.plateform.death2.PlateformDeath2View;
import com.jmpy.levels.plateform.death3.PlateformDeath3View;
import com.jmpy.levels.plateform.death4.PlateformDeath4View;
import com.jmpy.levels.plateform.doublejump0.PlateformDoubleJump0View;
import com.jmpy.levels.plateform.doublejump1.PlateformDoubleJump1View;
import com.jmpy.levels.plateform.reversegravity1.PlateformReverseGravity1View;
import com.jmpy.levels.plateform.reversegravity2.PlateformReverseGravity2View;
import com.jmpy.levels.plateform.reversegravity3.PlateformReverseGravity3View;
import com.jmpy.levels.plateform.reversegravity33.PlateformReverseGravity33View;
import com.jmpy.levels.plateform.tuto1.Tuto1View;
import com.jmpy.levels.plateform.tuto2.Tuto2View;
import com.jmpy.levels.plateform.tuto3.Tuto3View;
import com.jmpy.levels.plateform.tuto4.Tuto4View;
import com.jmpy.levels.plateform.tuto9.Tuto9View;
import com.jmpy.levels.plateform.walljump1.PlateformWallJump1View;
import jmpy.levels.plateform.walljump2.PlateformWallJump2View;

import java.util.Date;
import java.util.Vector;

public class MainActivity extends Activity {

	private Handler onClock = new Handler();
	private LevelView currentLevel;
	private RelativeLayout layout;
	public static final int FRAME_RATE = 33;
	private long previousFrameTime = (new Date()).getTime();
	private ScrollView scroll;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getSupportActionBar().hide();
        
        layout = new RelativeLayout(this);
        layout.setBackgroundColor(Color.GRAY);
        home = new Home(this);
        scroll = new ScrollView(this);
		scroll.setBackgroundColor(Color.BLACK);
		scroll.addView(home);
        layout.addView(scroll);
        setContentView(layout);
        onClock.postDelayed(updateClock, 0);
    }

    private Runnable updateClock = new Runnable() {
		@Override
		public void run() {
			if (currentLevel!=null) {
				Rect invalidateZone = new Rect(0, 0, layout.getWidth(), layout.getHeight());
				currentLevel.invalidate(invalidateZone);
				currentLevel.getControls().invalidate();
			}
			long newFrameTime = (new Date()).getTime();
	        onClock.postDelayed(this, FRAME_RATE - (newFrameTime-previousFrameTime));
	        previousFrameTime = newFrameTime;
		}
	};
	private Home home;
	private Vector<LevelDefinition> levels;
	
	public Vector<LevelDefinition> getLevels () {
		if (levels==null) {
			levels = new Vector<>();
			int index = 0;

			levels.add(new LevelDefinition<>(this, index++, "t1", Tuto1View.class, 0, 449, 289,index,0, BitmapFactory.decodeResource(this.getResources(), R.drawable.hint1)));
			levels.add(new LevelDefinition<>(this, index++, "t2", Tuto2View.class, 0, 317, 157, index,0,  BitmapFactory.decodeResource(this.getResources(), R.drawable.hint2)));
			levels.add(new LevelDefinition<>(this, index++, "t3", Tuto3View.class, 0, 359, 199, index,0, null));
			levels.add(new LevelDefinition<>(this, index++, "4", Tuto4View.class, 0, 481, 241, index+1, index, null));
			levels.add(new LevelDefinition<>(this, index++, "B1", Cart1View.class, 0, 591, 551, 0, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "5", Plateform1View.class, 0, 463, 223, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "6", Plateform2View.class, 0, 649, 369, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "7", Plateform4View.class, 0, 901, 581, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "8", Plateform5View.class, 0, 809, 489, index+1, index, null));
			levels.add(new LevelDefinition<>(this, index++, "b2", JetPack2View.class, 0, 803, 723, 0, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "t9", Tuto9View.class, 0, 1000,211, index, 0, BitmapFactory.decodeResource(this.getResources(), R.drawable.hint_wall_jump)));
			levels.add(new LevelDefinition<>(this, index++, "9", PlateformWallJump1View.class, 0, 371,211, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "10", PlateformWallJump2View.class, 0, 1399,839, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "11", PlateformDeath1View.class, 0, 822, 502, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "12", PlateformBounce0View.class, 0, 519, 359, index, 0, BitmapFactory.decodeResource(this.getResources(), R.drawable.hint_dash)));
			levels.add(new LevelDefinition<>(this, index++, "13", PlateformBounce1View.class, 0, 886, 566, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "14", PlateformBounce4View.class, 0, 1299, 899, index+1, index, null));
			levels.add(new LevelDefinition<>(this, index++, "B3", Cart1View.class, 0, 591, 551, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "15", PlateformDeath2View.class, 0, 1038, 718, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "16", PlateformDoubleJump0View.class, 0, 515, 315, index, 0, BitmapFactory.decodeResource(this.getResources(), R.drawable.hint_double_jump)));
			levels.add(new LevelDefinition<>(this, index++, "17", PlateformDoubleJump1View.class, 0, 831, 431, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "18", PlateformDeath3View.class, 0, 691, 451, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "20", PlateformReverseGravity1View.class, 0, 1021, 701, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "21", PlateformReverseGravity3View.class, 0, 1188, 708, index+1, index, null));
			levels.add(new LevelDefinition<>(this, index++, "B4", JetPack1View.class, 0, 609, 529, 0, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "22", PlateformDeath4View.class, 0, 618, 378, index, 0, null));
			levels.add(new LevelDefinition<>(this, index++, "23", PlateformReverseGravity2View.class, 0, 1271, 871, index, 0, null));

			levels.add(new LevelDefinition<>(this, index++, "33", PlateformReverseGravity33View.class, 0, 1758, 1118, 0, 0, null));
				
		}
		return levels;
	}

	public void openLevel(LevelDefinition<LevelView> levelDefinition) {
		layout.removeAllViews();
		try {
			this.currentLevel = (LevelView)levelDefinition.getContent().getConstructor(MainActivity.class, LevelDefinition.class).newInstance(this, levelDefinition);
			layout.addView(currentLevel);
			layout.addView(currentLevel.getControls());
		} catch (Exception e) {
			Log.e("MainActivity", "Not able to create level", e);
		}
	}

	public void showHome() {
		layout.removeViewAt(1);
		this.home.invalidate();
		layout.addView(this.scroll);
		
	}

	public RelativeLayout getLayout() {
		return layout;
	}
	
	
}
