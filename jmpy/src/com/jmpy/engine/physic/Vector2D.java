package com.jmpy.engine.physic;

import android.graphics.Point;
import android.graphics.PointF;

public class Vector2D {

	public double x;
	public double y;
	public Vector2D (int x, int y ) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public static Vector2D createFromTethaAngleAndNorm (double angle, double norm) {
		double x = Math.cos(angle)*norm;
		double y = Math.sin(angle)*norm;
		return new Vector2D(x, y);
	}
	
	public void offset(double x, double y) {
		this.x+=x;
		this.y+=y;
	}
	
	public void multiply(double x, double y) {
		this.x*=x;
		this.y*=y;
	}
	
	public void increase (Vector2D increased) {
		increased.offset(x, y);
	}
	public void increase (Point increased) {
		increased.offset((int)x,(int)y);
	}
	
	public double getNorm () {
		return Math.sqrt(x*x+y*y);
	}
	
	public double getThetaAngle () {
		return Math.atan2(y,x);
	}
	
	public double getScalarProduct (Vector2D compareWith) {
		return compareWith.x*x+compareWith.y*y;
	}
	

	public double getAngleWith (Vector2D compareWith) {
		return Math.atan2(compareWith.y,compareWith.x)-Math.atan2(y,x);
	}
	
	@Override
	public String toString () {
		return x+","+y;
	}

	public void reverse() {
		this.x = -x;
		this.y = -y;
	}

	public boolean isLeft() {
		return this.getThetaAngle() <= -5*Math.PI/6 || this.getThetaAngle() >= 5*Math.PI/6;
	}
	public boolean isRight() {
		return this.getThetaAngle() <= Math.PI/6 && this.getThetaAngle() >= -Math.PI/6;
	}
	public boolean isSmallLeft() {
		return this.getThetaAngle() <= -6*Math.PI/7 || this.getThetaAngle() >= 6*Math.PI/7;
	}
	public boolean isSmallRight() {
		return this.getThetaAngle() <= Math.PI/7 && this.getThetaAngle() >= -Math.PI/7;
	}
	public boolean isTop() {
		return this.getThetaAngle() > -5*Math.PI/6 && this.getThetaAngle() < -Math.PI/6;
	}
	public boolean isSmallTop() {
		return this.getThetaAngle() > -4*Math.PI/7 && this.getThetaAngle() < -3*Math.PI/7;
	}
	public boolean isBottom() {
		return this.getThetaAngle() <5*Math.PI/6 && this.getThetaAngle() > Math.PI/6;
	}
	public boolean isSmallBottom() {
		return this.getThetaAngle() <4*Math.PI/7 && this.getThetaAngle() > 3*Math.PI/7;
	}
	public boolean isFullLeft() {
		return this.x<0;
	}
	public boolean isFullRight() {
		return this.x>0;
	}
	public boolean isFullTop() {
		return this.y<0;
	}
	public boolean isFullBottom() {
		return this.y>0;
	}
}
