package com.jmpy.engine.component;

import java.util.Vector;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;

public abstract class ALand extends ASprite {

	protected int left;
	protected int top;

	private Bitmap floorBitmap;
	protected Matrix translation;
	
	protected Path zeroGHitBox = new Path();
	protected Path flagHitbox = new Path();
	protected Path bonusFlagHitbox = new Path();
	protected Path dieHitBox = new Path();
	protected Path bounceHitBox = new Path();
	protected Path wallJumpHitBox = new Path();
	protected Path reverseGravity = new Path();
	
	protected Vector<AnimatedLand> animatedLands = new Vector<AnimatedLand>();
	private Paint paint;
	

	public ALand(LevelView camera) {
		super(camera);
		translation = new Matrix();
		paint = new Paint();
		paint.setColor(Color.BLACK);
	}

	public Path getZeroGHitBox() {
		return zeroGHitBox;
	}

	public Path getFlagHitbox() {
		return flagHitbox;
	}

	public Path getBonusFlagHitbox() {
		return bonusFlagHitbox;
	}

	public void updateZeroG() {
		zeroGHitBox = new Path(hitBox);
		Matrix matrix = new Matrix();
		matrix.postTranslate(0, -1);
		zeroGHitBox.transform(matrix);
	}

	protected void draw(boolean showHitBox, Canvas canvas) {
		draw(showHitBox, canvas,true);
	}
	protected void draw(boolean showHitBox, Canvas canvas, boolean paintBackground) {
		if (floorBitmap!=null) {
			canvas.drawBitmap(floorBitmap, left, top, null);
			int borderSize = 20000;
			Rect bottomBlackMask = new Rect(left-borderSize, top+floorBitmap.getHeight(), left+floorBitmap.getWidth()+borderSize,  top+floorBitmap.getHeight()+borderSize);
			canvas.drawRect(bottomBlackMask , paint);
			Rect leftBlackMask = new Rect(left-borderSize, top-borderSize, left,  top+floorBitmap.getHeight()+borderSize);
			canvas.drawRect(leftBlackMask , paint);
			Rect rightBlackMask = new Rect(left+floorBitmap.getWidth(), top-borderSize, left+floorBitmap.getWidth()+borderSize,  top+floorBitmap.getHeight()+borderSize);
			canvas.drawRect(rightBlackMask , paint);
			Rect topBlackMask = new Rect(left-borderSize, top-borderSize, left+floorBitmap.getWidth()+borderSize,  top);
			canvas.drawRect(topBlackMask , paint);
		}
		
		if (showHitBox) {
			/*
			if(paintBackground)
				canvas.drawColor(Color.LTGRAY);
			*/
			Paint paint = new Paint();
			paint.setAntiAlias(false);
			paint.setColor(Color.CYAN);
			canvas.drawPath(reverseGravity, paint);
			paint.setColor(Color.GRAY);
			canvas.drawPath(getZeroGHitBox(), paint);
			paint.setColor(Color.YELLOW);
			canvas.drawPath(getBounceHitBox(), paint);
			paint.setColor(Color.MAGENTA);
			canvas.drawPath(getWallJumpHitBox(), paint);
			paint.setColor(Color.GREEN);
			canvas.drawPath(getFlagHitbox(), paint);
			paint.setColor(Color.LTGRAY);
			canvas.drawPath(getBonusFlagHitbox(), paint);
			paint.setColor(Color.RED);
			canvas.drawPath(getDieHitBox(), paint);
			paint.setColor(Color.BLACK);
			canvas.drawPath(getHitBox(), paint);
		}
		for (AnimatedLand animatedLand:animatedLands) {
			animatedLand.draw(canvas, showHitBox);
		}
	}

	public Path getDieHitBox() {
		return dieHitBox;
	}

	public Path getBounceHitBox() {
		return bounceHitBox;
	}

	public Path getWallJumpHitBox() {
		return wallJumpHitBox;
	}

	public Vector<AnimatedLand> getAnimatedLands() {
		return animatedLands;
	}

	public Path getReverseGravity() {
		return reverseGravity;
	}

	public Bitmap getFloorBitmap() {
		return floorBitmap;
	}

	public int getLeft() {
		return left;
	}

	public int getTop() {
		return top;
	}

	public void setFloorBitmap(Bitmap floorBitmap) {
		this.floorBitmap = floorBitmap;
	}

}
