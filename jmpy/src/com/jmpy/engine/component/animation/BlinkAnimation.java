package com.jmpy.engine.component.animation;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.Log;

public class BlinkAnimation extends Animation {
	private Matrix translation;
	private float ratio;

	public BlinkAnimation(int duration, int phase, float ratio) {
		super(duration, phase, 0);
		this.ratio = ratio;
	};
	public BlinkAnimation(int duration, int phase) {
		super(duration, phase, 0);
		this.ratio = (float) 0.5;
	}
	
	@Override
	public Matrix getTranslationMatrixByFrame(int nbFrame) {
		float currentFrame = getCurrentFrame(nbFrame);
		translation = new Matrix();
		if(currentFrame<duration*this.ratio) {
			translation.setTranslate(0, 0);
		} else {
			translation.setTranslate(-20000, 0);
		}
		return translation;
	}
}
