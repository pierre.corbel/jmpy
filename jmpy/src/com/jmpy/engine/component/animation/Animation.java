package com.jmpy.engine.component.animation;

import android.graphics.Matrix;
import android.graphics.PointF;

public abstract class Animation {

	protected int duration;
	protected int amplitude;
	int phase = 0;

	Animation (int duration, int phase, int amplitude) {
		this.duration = duration;
		this.amplitude= amplitude;
		this.phase = phase;
	}
	
	public abstract Matrix getTranslationMatrixByFrame(int nbFrame); 
	
	protected int getCurrentFrame(int nbFrame) {
		return (nbFrame+phase)%duration;
	}

}
