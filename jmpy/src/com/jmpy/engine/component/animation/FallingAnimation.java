package com.jmpy.engine.component.animation;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.Log;

public class FallingAnimation extends Animation {
	int fallingTime = 0;
	int waitingAfterFallTime = 0;
	int goingUpTime = 0;
	int waitingAfterGoingUpTime = 0;
	int phase = 0;
	private Matrix translation;

	public FallingAnimation(int amplitude, int phase, int fallingTime, int waitingAfterFallTime, int goingUpTime, int waitingAfterGoingUpTime) {
		super(fallingTime+waitingAfterFallTime+goingUpTime+waitingAfterGoingUpTime, phase, amplitude);
		this.fallingTime = fallingTime;
		this.waitingAfterFallTime = waitingAfterFallTime;
		this.goingUpTime = goingUpTime;
		this.waitingAfterGoingUpTime = waitingAfterGoingUpTime;
		this.phase = phase;
	}

	@Override
	public Matrix getTranslationMatrixByFrame(int nbFrame) {
		float currentFrame = getCurrentFrame(nbFrame);
		translation = new Matrix();
		float ratioY = 0;
		if(currentFrame<fallingTime) {
			ratioY = currentFrame/(fallingTime);
			ratioY*=ratioY;
		} else if (currentFrame<fallingTime+waitingAfterFallTime) {
			ratioY = 1;
		} else if (currentFrame<fallingTime+waitingAfterFallTime+goingUpTime){
			//int remainingTime = 
			int elapsedTime = (int) (fallingTime+waitingAfterFallTime);
			int remainingTime = goingUpTime;
			ratioY = 1-(currentFrame-elapsedTime)/goingUpTime;
		} else {
			ratioY = 0;
		}
		translation.setTranslate(0, amplitude * ratioY);
		return translation;
	}
}
