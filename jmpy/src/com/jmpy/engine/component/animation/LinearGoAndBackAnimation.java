package com.jmpy.engine.component.animation;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.Log;

public class LinearGoAndBackAnimation extends Animation {
	int waitingTime = 30;
	int phase = 0;
	private Matrix translation;

	public LinearGoAndBackAnimation(int duration, int amplitude, int waitingTime, int phase) {
		super((duration+waitingTime)*2, phase, amplitude);
		this.waitingTime = waitingTime;
		this.phase = phase;
	}

	@Override
	public Matrix getTranslationMatrixByFrame(int nbFrame) {
		float currentFrame = getCurrentFrame(nbFrame);
		translation = new Matrix();
		float ratio = 0;
		if(currentFrame<waitingTime) {
			ratio = 0;
		} else if (currentFrame < duration/2) {
			ratio = (currentFrame-waitingTime)/(duration/2-waitingTime);
		} else if(currentFrame<duration/2+waitingTime) {
			ratio = 1;
		} else {
			ratio = 2- (currentFrame-2*waitingTime)/(duration/2-waitingTime);
		}
		translation.setTranslate(amplitude * (ratio), 0);
		return translation;
	}
}
