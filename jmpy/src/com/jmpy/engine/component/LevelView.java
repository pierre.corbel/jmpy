		package com.jmpy.engine.component;

import android.annotation.SuppressLint;
import com.jmpy.MainActivity;
import com.jmpy.engine.physic.BounceData;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.data.db.LevelDbHelper;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.View;

		/**
 * @author pierre
 *
 */
public abstract class LevelView extends View {
	private static final double MAXIMUM_DELTA_ZOOM = 0.005;
	public static final double LOWER_ZOOM = 0.6;
			private static final double CONTROL_HEIGHT = 170;
			protected Ball ball;
	protected ALand land;
	public Vector2D pushAccelerate;
	private Vector2D canvasCenterOnScreen;
	private Point initialBallCenter;
	private float zoom;
	protected Controls controls;
	private float gravity;
	private int nbFrameSinceInTheSky = 0;
	private Paint paint = new Paint();
	private boolean isFinish = false;
	private boolean isStarted = false;
	protected int nbJump = 0;
	private int time = 0;
	private LevelDefinition<LevelView> levelDefinition;
	private BounceData wallJumpData;
	private double previousSpeedZoom = LOWER_ZOOM;
	private int nbFrameSinceSlower = 0;
	private BounceData reverseGravity;
	private Background background= null;
	private BounceData previousWallJumpBounceData;
			private Vector2D cameraCenterForBackgroundOnTopLeftCorner;
			private Vector2D cameraCenterForBackgroundOnBottomRightCorner;

			public BounceData getPreviousWallJumpBounceData() {
		return previousWallJumpBounceData;
	}

	public LevelView(MainActivity mainActivity, LevelDefinition<LevelView> levelDefinition) {
		super(mainActivity);
		background =new Background(mainActivity, this, null, -240, -240);
		this.levelDefinition = levelDefinition;
		init();
	}
	
	protected abstract void init();

	protected void init(Ball ball, ALand land, Controls controls, float gravity) {
		this.ball = ball;
		this.initialBallCenter = new Point((int)ball.getCenter().x, (int)ball.getCenter().y);
		this.land = land;
		this.controls = controls;
		this.gravity = gravity;
		zoom = (float) 1;
	}
	
	public float getGravity() {
		return gravity;
	}

	protected void init(Ball ball, ALand land, Controls controls, float gravity, float zoom) {
		this.ball = ball;
		this.initialBallCenter = new Point((int)ball.getCenter().x, (int)ball.getCenter().y);
		this.land = land;
		this.controls = controls;
		this.gravity = gravity;
		this.zoom = zoom;
	}
	
	protected Vector2D getBallPositionOnScreen (int width, int height) {
		return new Vector2D(width/2,height/2);
	}
	
	protected void manageAcceleration (int nbFrameInTheSky, BounceData wallJumpData) {
		if (controls.getJumpControlVector()!=null && (nbFrameInTheSky < 5 || wallJumpData!=null)) {
			this.nbJump  ++;
			pushAccelerate = controls.getJumpControlVector();
			Log.d("LevelView", "Manage accélération!"+pushAccelerate.getNorm());
		}
	}

	@Override
	protected void onDraw(Canvas canvas) {
		if (controls.getJumpControlVector()!=null || controls.getMoveControlVector()!=null)
			isStarted = true;
		if (!isFinish && isStarted)
			this.time ++;
		
		if (canvasCenterOnScreen ==null)
			canvasCenterOnScreen = getBallPositionOnScreen(getWidth(), getHeight());
		canvas.drawColor(Color.TRANSPARENT);
		ball.computeHitBox();

		
		BounceData bounceData = ball.bounce(land);
		BounceData zeroGBounceData = ball.checkZeroG(land);
		BounceData touchFlagBounceData = ball.checkTouchFlag(land);
		BounceData touchBonusFlagBounceData = ball.checkBonusTouchFlag(land);
		BounceData dieData = ball.checkDie(land);
		wallJumpData = ball.checkWallJump(land);
		reverseGravity = ball.checkReverseGravity(land);
		
		
		for(AnimatedLand animatedLand: land.getAnimatedLands()) {
			if(bounceData==null)
				bounceData = ball.bounce(animatedLand);
			if(zeroGBounceData==null)
				zeroGBounceData = ball.checkZeroG(animatedLand);
			if(touchFlagBounceData==null)
				touchFlagBounceData = ball.checkBonusTouchFlag(animatedLand);
			if(dieData==null)
				dieData = ball.checkDie(animatedLand);
			if(wallJumpData==null)
				wallJumpData = ball.checkWallJump(animatedLand);
			if(reverseGravity==null)
				reverseGravity = ball.checkReverseGravity(animatedLand);
		}
		
		
		manageAcceleration (nbFrameSinceInTheSky, wallJumpData);
		
		if (dieData!=null && dieData.intersection.width() >3 && dieData.intersection.height() >3) {
			die();
		}
		
		if (wallJumpData!=null && bounceData!=null) {
			this.ball.getSpeed().x = 0;
			this.ball.getSpeed().y = 0;
			previousWallJumpBounceData = bounceData;
		}
		if(wallJumpData==null)
			previousWallJumpBounceData = null;
		if (touchFlagBounceData!=null && !isFinish)
			finishLevel(false);
		if (touchBonusFlagBounceData!=null && !isFinish)
			finishLevel(true);
		
		
		
		if(bounceData==null){
			if (zeroGBounceData==null) {
				float appliedGravity = this.gravity;
				if(wallJumpData!=null&&ball.getSpeed().isBottom())
					appliedGravity/=2;
				if(reverseGravity!=null)
					appliedGravity=-appliedGravity;
				ball.accelerate(new Vector2D(0,appliedGravity));
			}
			if (pushAccelerate!=null) {
				ball.accelerate(pushAccelerate);
				pushAccelerate = null;
			}
			
		}
		if(zeroGBounceData!=null){
			if (zeroGBounceData.ColisionNormaleVector.isTop())
				nbFrameSinceInTheSky = 0;
		}
		ball.move();
		defineCameraCenterOn(canvas);


		if (background!=null) {
			background.draw(canvas);
			//background.setL
		}
		land.draw(canvas);
		ball.draw(canvas);

		paint.reset();
		paint.setColor(Color.RED);
		super.onDraw(canvas);
		nbFrameSinceInTheSky++;
	}

	private void defineCameraCenterOn(Canvas canvas) {

		canvas.translate((float) canvasCenterOnScreen.x,(float) canvasCenterOnScreen.y);
		//float smallMobileRatio = (float) 480/((MainActivity)getContext()).getLayout().getWidth();
		float resolutionZoom = getResources().getDisplayMetrics().density;
		float applySpeedZoom = zoom*resolutionZoom*calculateNextSpeedZoom();

		canvas.scale(applySpeedZoom, applySpeedZoom);


		double cameraCenterOnX = ball.getCenter().x;
		double cameraCenterOnY = ball.getCenter().y;
		if(land.getFloorBitmap()!=null) {
			if(cameraCenterForBackgroundOnTopLeftCorner ==null|| cameraCenterForBackgroundOnBottomRightCorner ==null) {
				cameraCenterForBackgroundOnTopLeftCorner = new Vector2D(canvasCenterOnScreen.x/applySpeedZoom, canvasCenterOnScreen.y/applySpeedZoom +land.getTop());
				cameraCenterForBackgroundOnBottomRightCorner = new Vector2D(land.getLeft() - canvasCenterOnScreen.x/applySpeedZoom + land.getFloorBitmap().getWidth(), land.getTop() - canvasCenterOnScreen.y/applySpeedZoom + land.getFloorBitmap().getHeight() + CONTROL_HEIGHT/applySpeedZoom);
			}
			if (land.getFloorBitmap().getWidth()< getWidth()/applySpeedZoom ) {
				cameraCenterOnX = cameraCenterForBackgroundOnTopLeftCorner.x + (cameraCenterForBackgroundOnBottomRightCorner.x - cameraCenterForBackgroundOnTopLeftCorner.x)/2;
			} else if (cameraCenterOnX< cameraCenterForBackgroundOnTopLeftCorner.x) {
				cameraCenterOnX = cameraCenterForBackgroundOnTopLeftCorner.x;
			} else if (cameraCenterOnX> cameraCenterForBackgroundOnBottomRightCorner.x) {
				cameraCenterOnX = cameraCenterForBackgroundOnBottomRightCorner.x;
			}

			if (land.getFloorBitmap().getHeight()< getHeight()/applySpeedZoom - CONTROL_HEIGHT/applySpeedZoom ) {
				cameraCenterOnY = cameraCenterForBackgroundOnTopLeftCorner.y + (cameraCenterForBackgroundOnBottomRightCorner.y - cameraCenterForBackgroundOnTopLeftCorner.y)/2;
			} else if (cameraCenterOnY< cameraCenterForBackgroundOnTopLeftCorner.y) {
				cameraCenterOnY = cameraCenterForBackgroundOnTopLeftCorner.y;
			} else if (cameraCenterOnY> cameraCenterForBackgroundOnBottomRightCorner.y) {
				cameraCenterOnY = cameraCenterForBackgroundOnBottomRightCorner.y;
			}
		}
		Vector2D cameraCenterOn = new Vector2D(cameraCenterOnX, cameraCenterOnY);
		canvas.translate(-(float)cameraCenterOn.x, -(float)cameraCenterOn.y);
	}

	private float calculateNextSpeedZoom() {
		/*
		double expectedSpeedZoom = LOWER_ZOOM /((float) (
				1+ 
				0.5*Math.abs(getBall().getSpeed().getNorm())
				/getBall().getMaxSpeed()));
		double applySpeedZoom;
		if(expectedSpeedZoom<=previousSpeedZoom) {
			this.nbFrameSinceSlower = 0;
		} else {
			this.nbFrameSinceSlower ++;
		}
		if(expectedSpeedZoom<=previousSpeedZoom || this.nbFrameSinceSlower>100) {
			applySpeedZoom = expectedSpeedZoom;
			if(Math.abs(previousSpeedZoom-expectedSpeedZoom)>MAXIMUM_DELTA_ZOOM)
				applySpeedZoom = expectedSpeedZoom = previousSpeedZoom>expectedSpeedZoom ? (float) (previousSpeedZoom-MAXIMUM_DELTA_ZOOM):(float) (previousSpeedZoom+MAXIMUM_DELTA_ZOOM);
				
		} else {
			applySpeedZoom = previousSpeedZoom;
		}

		previousSpeedZoom = applySpeedZoom;
		*/
		return (float)LOWER_ZOOM;//(float)applySpeedZoom;
	}

	public BounceData getWallJumpData() {
		return wallJumpData;
	}

	public BounceData getReverseGravity() {
		return reverseGravity;
	}

	public void die() {
		this.ball.getSpeed().x = 0;
		this.ball.getSpeed().y = 0;
		this.ball.getCenter().x = initialBallCenter.x;
		this.ball.getCenter().y = initialBallCenter.y;
		nbJump = 0;
		time = 0;
		isStarted = false;
		for(AnimatedLand animatedLand: this.land.getAnimatedLands()) {
			animatedLand.resetAnimationFrame();
		}
	}

	private void finishLevel(boolean isBonusFinish) {
		this.isFinish  = true;
		boolean isWinned;
		if (isBonusFinish)
			isWinned = this.levelDefinition.finishBonusLevel(nbJump, time);
		else
			isWinned = this.levelDefinition.finishLevel(nbJump, time);
		
		if (isWinned) {
			controls.showWinPopup(isBonusFinish);
		} else {
			controls.showLosePopup();
		}
	}

	
	public Vector2D getCanvasCenterOnScreen() {
		return canvasCenterOnScreen;
	}

	public Controls getControls () {
		return controls;
	}


	public int getNbFrameSinceInTheSky() {
		return nbFrameSinceInTheSky;
	}


	public int getTime() {
		return time;
	}

	public void stopTheBall() {
		this.ball.getSpeed().x = 0;
		this.ball.getSpeed().y = 0;
	}
	public int getNbJump() {
		return nbJump;
	}

	public LevelDefinition<LevelView> getLevelDefinition() {
		return levelDefinition;
	}

	public Ball getBall() {
		return ball;
	}

}