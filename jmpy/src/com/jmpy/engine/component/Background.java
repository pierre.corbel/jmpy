package com.jmpy.engine.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;

import com.example.jmpy.R;

class Background {
	
	public Bitmap background;
	private int top;
	private int left;
	private LevelView levelView;
	private Matrix translation;
	
	public Background(Context context, LevelView levelView, Bitmap background, int left, int top) {
		this.levelView = levelView;
		this.top = -200;
		this.left = -200;
		this.background = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
		translation = new Matrix();
		translation.setTranslate(-200, -200);
	}
	
	public void draw(Canvas canvas) {
		//canvas.drawBitmap(background, left+(int)(this.levelView.getBall().getCenter().x/2), top+(int)(this.levelView.getBall().getCenter().y/2), null);
	}

	public void setTopTranslate(int top) {
		this.top = top;
	}

	public void setLeftTranslate(int left) {
		this.left = left;
	}

	public int getTop() {
		return top;
	}

	public int getLeft() {
		return left;
	}

	public Bitmap getBitmap() {
		return background;
	}
}
