package com.jmpy.engine.component;

import com.jmpy.MainActivity;
import com.jmpy.engine.physic.Vector2D;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.shapes.Shape;
import android.view.View;

public abstract class ASprite {

	private Vector2D center= new Vector2D(0,0);
	protected Vector2D speed = new Vector2D(0, 0);
	protected Path hitBox = new Path();
	private Vector2D bounceFactor = new Vector2D(1, 1);
	protected int maxSpeed = 30;
	protected LevelView levelView;
	
	public ASprite(LevelView camera) {
		this.levelView = camera;
	}
	
	public void draw (Canvas canvas) {
		draw(canvas, false);
	}
	public abstract void draw (Canvas canvas, boolean showHitBox);
	
	public void move() {
		speed.increase(center);
	}
	
	public void accelerate (Vector2D acceleration) {
		if(acceleration==null)
			return;
		acceleration.increase(speed);
		if (speed.x > this.maxSpeed)
			speed.x = this.maxSpeed;
		if (speed.x < -this.maxSpeed)
			speed.x = -this.maxSpeed;
		/*
		if (speed.y > this.maxSpeed)
			speed.y = this.maxSpeed;
		if (speed.y < -this.maxSpeed)
			speed.y = -this.maxSpeed;
		 */
	}

	public Vector2D getSpeed() {
		return speed;
	}


	
	public void setSpeed(Vector2D speed) {
		this.speed = speed;
	}

	public Vector2D getCenter() {
		return center;
	}
	public void setCenter(Point center) {
		this.center = new Vector2D(center.x,center.y);
	}

	public Path getHitBox() {
		return hitBox;
	}

	public void setHitBox(Path hitBox) {
		this.hitBox = hitBox;
	}
	
	public Vector2D getBounce() {
		return bounceFactor;
	}
	
	public void setBounce(Vector2D bounceFactor) {
		this.bounceFactor = bounceFactor;
	}

	public void setMaxSpeed(int maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	public int getMaxSpeed() {
		return this.maxSpeed;
	}
	
	
}
