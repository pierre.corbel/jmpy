package com.jmpy.engine.component;

import com.jmpy.engine.component.animation.Animation;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;

public class AnimatedLand extends ALand {

	private Animation animation;
	private int currentFrame = 0;
	private Matrix translation;

	public AnimatedLand(LevelView camera, Animation animation) {
		super(camera);
		this.animation= animation;
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		currentFrame++;
		this.translation = null;
		super.draw(showHitBox, canvas, false);
	}
	
	@Override
	public Path getDieHitBox() {
		return getAnimatedPath(super.getDieHitBox());
	}

	@Override
	public Path getHitBox() {
		return getAnimatedPath(super.getHitBox());
	}
	
	@Override
	public Path getBounceHitBox() {
		return getAnimatedPath(super.getBounceHitBox());
	}

	@Override
	public Path getBonusFlagHitbox() {
		return getAnimatedPath(super.getBonusFlagHitbox());
	}
	
	@Override
	public Path getFlagHitbox() {
		return getAnimatedPath(super.getFlagHitbox());
	}
	
	@Override
	public Path getWallJumpHitBox() {
		return getAnimatedPath(super.getWallJumpHitBox());
	}
	
	
	public Path getAnimatedPath (Path path) {
		if(path==null)
			return null;
		Path newPath = new Path(path);
		if(this.translation==null)
			this.translation = animation.getTranslationMatrixByFrame(currentFrame);
		newPath.transform(this.translation);
		return newPath;
	}

	public void resetAnimationFrame() {
		currentFrame = 0;
	}
}
