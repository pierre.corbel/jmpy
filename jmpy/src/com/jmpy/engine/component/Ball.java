package com.jmpy.engine.component;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Bitmap.Config;
import android.util.Log;
import android.view.View;

import com.example.jmpy.R;
import com.jmpy.engine.physic.BounceData;
import com.jmpy.engine.physic.Vector2D;

public class Ball extends ASprite {

	protected Paint paint;
	private Bitmap ball;
	protected int hitBoxRadius = 10;
	

	public Ball(LevelView levelView, Point center, int maxSpeed, Vector2D bounceFactor) {
		super(levelView);
		paint = new Paint();
        this.setCenter(center);
        this.setBounce(bounceFactor);
        this.setMaxSpeed(maxSpeed);
		ball = BitmapFactory.decodeResource(levelView.getResources(), R.drawable.ball);
		
	}
    
	public void computeHitBox () {
		this.getHitBox().reset();
		this.getHitBox().addCircle((float)getCenter().x, (float)getCenter().y, hitBoxRadius, Path.Direction.CCW);
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		canvas.drawBitmap(ball, (float)getCenter().x-hitBoxRadius, (float)getCenter().y-hitBoxRadius, null);

		//canvas.drawPath(getHitBox(), paint);
		if (showHitBox)
			canvas.drawPath(getHitBox(), paint);
	}

	
	public BounceData bounce (ALand bounceSurface) {
		BounceData bounceData =  this.getCollisonVector2D(bounceSurface.getHitBox());
		if (bounceData!=null) {
			Vector2D collisionDirection = bounceData.ColisionNormaleVector;
			Rect intersection = bounceData.intersection;
			
			Vector2D speedVector = this.getSpeed();
			double totalSpeed = this.getSpeed().getNorm();
			// the ball penetrate to much in the colision, we must reverse vector between colision epicentre and ball center
			
			if (intersection.width()*intersection.height() == this.hitBoxRadius*this.hitBoxRadius*4)
				collisionDirection = new Vector2D(-this.getSpeed().x, -this.getSpeed().y);
			
			collisionDirection.reverse();
			double newAngle = getColisionAngle(collisionDirection, speedVector);
			Vector2D newSpeed = Vector2D.createFromTethaAngleAndNorm(newAngle, totalSpeed);
			newSpeed.reverse();

			if(this.checkBounce(bounceSurface)==null) {
				// 0 means a frontal colision, 1 means a lateral collision
				// The slow down bounce factor is less important on lateral collision
				double frontOrlateralColisionRatio = (Math.PI-Math.abs(collisionDirection.getAngleWith(newSpeed))) / (Math.PI/2);
				//frontOrlateralColisionRatio = 0;
				frontOrlateralColisionRatio = 0;
				double ratioX = getBounce().x*bounceSurface.getBounce().x;
				this.getSpeed().x = newSpeed.x*ratioX;
				if(collisionDirection.isSmallLeft() || collisionDirection.isSmallRight()) {
					this.getSpeed().x /= 10;
					this.getSpeed().y = newSpeed.y;
				} else {
					double ratioY = getBounce().y*bounceSurface.getBounce().y;
					this.getSpeed().y = newSpeed.y*ratioY;
				}
			} else {
				this.getSpeed().x = newSpeed.x;
				this.getSpeed().y = newSpeed.y;
			}

			
			if (intersection.width()>intersection.height()) {
				double expulseNorm = Math.sin(collisionDirection.getThetaAngle()) > 0?intersection.height():-intersection.height();	
				getCenter().y -= expulseNorm;
			} else {
				double expulseNorm = Math.cos(collisionDirection.getThetaAngle()) > 0?intersection.width():-intersection.width();		
				getCenter().x -= expulseNorm;
			}
			
			return bounceData;
		}
		return null;
	}

	protected double getColisionAngle(Vector2D collisionDirection,
			Vector2D speedVector) {
		double newAngle = speedVector.getThetaAngle() + 2*speedVector.getAngleWith(collisionDirection);
		return newAngle;
	}
	
	protected BounceData getCollisonVector2D(Path testedHitbox) {
		RectF testedSpriteBoundary = new RectF();
		testedHitbox.computeBounds(testedSpriteBoundary, false);
		RectF myBoundary = new RectF();
		this.getHitBox().computeBounds(myBoundary, false);
		if (testedSpriteBoundary.intersect(myBoundary)) {
			// now testedSpriteBoundary contains maximum common parts
			// we used it for minimize intersection calcul;
			Rect minimalClipping = new Rect((int)testedSpriteBoundary.left, (int)testedSpriteBoundary.top, (int)testedSpriteBoundary.right, (int)testedSpriteBoundary.bottom);
			Region testedRegion = new Region();
			testedRegion.setPath(testedHitbox, new Region(minimalClipping));
			Region myRegion = new Region();
			myRegion.setPath(getHitBox(), new Region(minimalClipping));
			if (myRegion.op(minimalClipping, testedRegion, Region.Op.INTERSECT)) {

				//this.getIntersect(testedSprite, myRegion.getBounds());
				Rect penetrationZone = testedRegion.getBounds();
				PointF epicentre = new PointF(penetrationZone.left + penetrationZone.width()/2, penetrationZone.top + penetrationZone.height()/2);

				Vector2D boundVector = new Vector2D(getCenter().x - epicentre.x, getCenter().y - epicentre.y);
				RectF bounceZone = new RectF((float)getCenter().x, (float)getCenter().y, epicentre.x, epicentre.y);
				
				BounceData result = new BounceData();
				result.ColisionNormale = bounceZone;
				result.ColisionNormaleVector = boundVector;
				result.intersection = penetrationZone;
				return result;
			}
		}
		return null;
	}

	public BounceData checkZeroG(ALand land) {
		return getCollisonVector2D(land.getZeroGHitBox());
	}
	
	public BounceData checkTouchFlag(ALand land) {
		return getCollisonVector2D(land.getFlagHitbox());
	}

	public BounceData checkBonusTouchFlag(ALand land) {
		return getCollisonVector2D(land.getBonusFlagHitbox());
	}

	public BounceData checkDie(ALand land) {
		return getCollisonVector2D(land.getDieHitBox());
	}

	public BounceData checkWallJump(ALand land) {
		return getCollisonVector2D(land.getWallJumpHitBox());
	}

	public BounceData checkBounce(ALand land) {
		return getCollisonVector2D(land.getBounceHitBox());
	}

	public BounceData checkReverseGravity(ALand land) {
		return getCollisonVector2D(land.getReverseGravity());
	}
}