package com.jmpy.layout;

import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.jmpy.engine.physic.Vector2D;
import com.jmpy.levels.plateform.PlateformControl;

public class OnControlTouch implements OnTouchListener {

	public enum ACCELERATION_TYPE {
		MOVE,
		JUMP
	};

	public enum EVENT_TYPE {
		MOVE,
		DOWN,
		UP
	};
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		Controls controlView= (Controls) v;
		if(controlView.isShowHint()) {
			controlView.setShowHint(false);
			return true;
		}
		
		// get pointer index from the event object
		int pointerIndex = event.getActionIndex();

		// get pointer ID
		int pointerId = event.getPointerId(pointerIndex);

		// get masked (not specific to a pointer) action
		int maskedAction = event.getActionMasked();
		switch (maskedAction) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_POINTER_DOWN: {
			PointF point = new PointF();
			point.x = event.getX(pointerIndex);
			point.y = event.getY(pointerIndex);
			controlView.getmActivePointers().put(pointerId, point);
			evaluateNewPosition(point, controlView, EVENT_TYPE.DOWN);
			break;
		}

		case MotionEvent.ACTION_MOVE: { // a pointer was moved
			for (int size = event.getPointerCount(), i = 0; i < size; i++) {
				PointF point = controlView.getmActivePointers().get(event.getPointerId(i));
				if (point != null) {
					point.x = event.getX(i);
					point.y = event.getY(i);
					evaluateNewPosition(point, controlView, EVENT_TYPE.MOVE);
				}
			}
			break;
		}
	    case MotionEvent.ACTION_UP:
	    case MotionEvent.ACTION_POINTER_UP:
		case MotionEvent.ACTION_CANCEL: {
			PointF point =  controlView.getmActivePointers().get(pointerId);
			if(point!=null){
				if (controlView.getResetZone()!=null && controlView.getResetZone().contains((int)point.x, (int)point.y)) {
					controlView.resetLevel();
				} else if (controlView.getBackZone()!=null && controlView.getBackZone().contains((int)point.x, (int)point.y)) {
					controlView.showHome();
	        	} else {
	        		clearPosition(point, controlView);
	        	}
			}
			controlView.getmActivePointers().remove(pointerId);
			break;
		}
		}
		return true;
	}

	protected void clearPosition(PointF point, Controls controlView) {
		controlView.calculateFuturPushAcceleration(null, ACCELERATION_TYPE.JUMP);
	}

	protected void evaluateNewPosition(PointF point, Controls controlView, EVENT_TYPE eventType) {
		if (controlView.getJumpHitBox().contains((int)point.x, (int)point.y)) {
			Vector2D clickVector = new Vector2D(controlView.getJumpCenterHitBox().x-point.x, controlView.getJumpCenterHitBox().y-point.y);
			controlView.calculateFuturPushAcceleration(clickVector, ACCELERATION_TYPE.JUMP);
		}
	}
}