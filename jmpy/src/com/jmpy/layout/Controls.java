package com.jmpy.layout;

import com.example.jmpy.R;
import com.jmpy.MainActivity;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.OnControlTouch.ACCELERATION_TYPE;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

public abstract class Controls extends View {

    protected int BUTTON_PADDING = 25;
    private Bitmap resetButton;
    public Rect resetZone;
    public Rect backZone;
    protected LevelView levelView;

    protected Vector2D moveControlVector = null;
    protected Vector2D jumpControlVector;
    protected Vector2D previousJumpControlVector;
    protected Paint paint = new Paint();

    protected float maxAccelerate = 6;

    protected int maxDistance = 60;
    private Bitmap backButton;

    protected PointF jumpCenterHitBox;
    protected Rect jumpHitBox;

    boolean showHint = false;

    private SparseArray<PointF> mActivePointers = new SparseArray<PointF>();
    private int[] colors = {Color.BLUE, Color.GREEN, Color.MAGENTA,
            Color.BLACK, Color.CYAN, Color.GRAY, Color.RED, Color.DKGRAY,
            Color.LTGRAY, Color.YELLOW};
    protected float zoomResolution;

    public Controls(Context context, LevelView camera, float maxAccelerate,
                    int maxDistance) {
        super(context);
        this.levelView = camera;
        resetButton = BitmapFactory.decodeResource(getResources(),
                R.drawable.refresh);
        backButton = BitmapFactory.decodeResource(getResources(),
                R.drawable.back);
        this.maxAccelerate = maxAccelerate;
        setOnTouchListener(new OnControlTouch());
        showHint = this.levelView.getLevelDefinition().getHint() != null;
        this.zoomResolution = getResources().getDisplayMetrics().density;
        this.maxDistance = (int) (maxDistance * zoomResolution);
        BUTTON_PADDING = (int) (BUTTON_PADDING * this.zoomResolution);
    }

    public Rect getResetZone() {
        return resetZone;
    }

    public Rect getBackZone() {
        return backZone;
    }

    public LevelView getLevelView() {
        return levelView;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!showHint) {
            paint.reset();
            drawStandartControl(canvas);
        } else {
            Bitmap hint = this.levelView.getLevelDefinition().getHint();
            canvas.drawColor(Color.argb(128, 0, 0, 0));
            canvas.drawBitmap(hint, getWidth() / 2 - hint.getWidth() / 2, getHeight() / 2 - hint.getHeight() / 2, null);
        }

    }


    public boolean isShowHint() {
        return showHint;
    }


    public void setShowHint(boolean showHint) {
        this.showHint = showHint;
    }

    private void drawDebugTouchCircle(Canvas canvas) {
        for (int size = mActivePointers.size(), i = 0; i < size; i++) {
            PointF point = mActivePointers.valueAt(i);
            paint.setColor(colors[i % 9]);
            canvas.drawCircle(point.x, point.y, 40, paint);
        }
    }

    public PointF getJumpCenterHitBox() {
        return jumpCenterHitBox;
    }

    public Rect getJumpHitBox() {
        return jumpHitBox;
    }

    protected void drawStandartControl(Canvas canvas) {
        if (resetZone == null)
            resetZone = new Rect(BUTTON_PADDING, BUTTON_PADDING, BUTTON_PADDING + resetButton.getWidth(),
                    BUTTON_PADDING + resetButton.getHeight());
        if (backZone == null)
            backZone = new Rect(getWidth() - backButton.getHeight() - BUTTON_PADDING, BUTTON_PADDING,
                    getWidth() - BUTTON_PADDING, BUTTON_PADDING + resetButton.getHeight());
        // canvas.rotate(90);
        // canvas.translate(0, -getWidth());
        canvas.drawBitmap(resetButton, BUTTON_PADDING, BUTTON_PADDING, null);
        canvas.drawBitmap(backButton, getWidth() - BUTTON_PADDING - backButton.getWidth(), BUTTON_PADDING, null);
/*
        int maxTime = this.levelView.getLevelDefinition().getMaxTime();
		int maxJumps = this.levelView.getLevelDefinition().getNbJumpMax();
		paint.setTextSize(12*this.zoomResolution);
		paint.setColor(Color.BLACK);
		canvas.drawText("Nb Jump(s): " + this.levelView.getNbJump()
				+ (maxJumps > 0 ? "/" + maxJumps : ""),
				BUTTON_PADDING + resetButton.getWidth() + BUTTON_PADDING, 5*BUTTON_PADDING, paint);
		canvas.drawText("Time: " + convertFrameToSecond(this.levelView.getTime())+"s"
				+ (maxTime > 0 ? "/" + convertFrameToSecond(maxTime)+"s" : ""),
				BUTTON_PADDING + resetButton.getWidth() + BUTTON_PADDING, 3*BUTTON_PADDING, paint);
				*/
    }

    public void calculateFuturPushAcceleration(Vector2D pushDistance, ACCELERATION_TYPE move) {
        Vector2D controlVector;
        if (pushDistance == null) {
            controlVector = null;
        } else {
            double distance = pushDistance.getNorm() < maxDistance ? pushDistance.getNorm() : maxDistance;
            double power = distance / maxDistance * maxAccelerate;
            controlVector = Vector2D.createFromTethaAngleAndNorm(
                    pushDistance.getThetaAngle(), power);
        }
        if (move == ACCELERATION_TYPE.JUMP) {
            boolean userReleaseJumpButton = controlVector == null && previousJumpControlVector != null;
            jumpControlVector = userReleaseJumpButton ? previousJumpControlVector : null;
            previousJumpControlVector = controlVector;
        } else {
            moveControlVector = controlVector;
        }
    }

    public void showHome() {
        ((MainActivity) getContext()).showHome();
    }

    public void resetLevel() {
        levelView.die();
    }

    public Vector2D getMoveControlVector() {
        return moveControlVector;
    }

    public Vector2D getJumpControlVector() {
        return jumpControlVector;
    }

    public void showWinPopup(boolean isBonusFinish) {
        AlertDialog.Builder winDialog = new AlertDialog.Builder(getContext());
        String message = "";
        if (isBonusFinish)
            message += "LEVEL BONUS UNBLOCKED, ";
        message += "Well done!\n";

        int maxJumps = this.levelView.getLevelDefinition().getNbJumpMax();
        if (maxJumps > 0) {
            message += "Nb Jump(s): " + this.levelView.getNbJump() + (maxJumps > 0 ? "/" + maxJumps : "") + "\n";
            //if (this.levelView.getLevelDefinition().isJustPassJumpRecord())
            //	message += "\nNEW RECORD!!!";
            //else {
            //	message +="\nYour record: "+this.levelView.getLevelDefinition().getRecordJump();
            //}
        } else {
            message += "Time: " + convertFrameToSecond(this.levelView.getTime()) + "s";
            int goldTime = this.levelView.getLevelDefinition().getGoldTime();
            int silverTime = this.levelView.getLevelDefinition().getSilverTime();
            int passTime = this.levelView.getLevelDefinition().getMaxTime();
            message += "\nGold time (" + convertFrameToSecond(goldTime) + "s)";
            if (goldTime >= levelView.getTime()) {
                message += " <";
            }
            message += "\nSilver time (" + convertFrameToSecond(silverTime) + "s)";
            if (silverTime >= levelView.getTime() && goldTime < levelView.getTime()) {
                message += " <";
            }
            message += "\nBronze time (" + convertFrameToSecond(passTime) + "s)";
            if (passTime >= levelView.getTime() && silverTime < levelView.getTime()) {
                message += " <";
            }
            if (this.levelView.getLevelDefinition().isJustPassJumpRecord())
                message += "\n\nNEW RECORD!!!";
            else {
                message += "\n\nYour record: " + convertFrameToSecond(this.levelView.getLevelDefinition().getRecordTime()) + "s";
            }
        }


        winDialog.setMessage(message);
        winDialog.setPositiveButton("New level?",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((MainActivity) getContext()).showHome();
                    }
                });
        winDialog.setNegativeButton("Retry?",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((MainActivity) getContext()).openLevel(levelView
                                .getLevelDefinition());
                    }
                });
        winDialog.setOnCancelListener(new OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                ((MainActivity) getContext()).showHome();
            }
        });
        AlertDialog theDialog = winDialog.create();
        theDialog.show();
    }

    private String convertFrameToSecond(int nbFrame) {
        double nbSeconds = (double) (nbFrame * MainActivity.FRAME_RATE) / 1000;
        return String.valueOf(nbSeconds);
    }

    public void showLosePopup() {
        AlertDialog.Builder winDialog = new AlertDialog.Builder(getContext());
        String message = "You didn't pass this level...\n";
        int maxTime = this.levelView.getLevelDefinition().getMaxTime();
        int maxJumps = this.levelView.getLevelDefinition().getNbJumpMax();
        //message += "Nb Jump(s): " + this.levelView.getNbJump()+ (maxJumps > 0 ? "/" + maxJumps : "") + "\n";
        message += "Time: " + convertFrameToSecond(this.levelView.getTime()) + "s"
                + (maxTime > 0 ? "/" + convertFrameToSecond(maxTime) : "") + "\n";
        message += "\nYour records\n";
        //message += "Nb Jump(s): "+ this.levelView.getLevelDefinition().getRecordJump();
        //if (this.levelView.getLevelDefinition().isJustPassJumpRecord())
        //	message += " NEW RECORD!!!";
        message += "\n";
        message += "Time: "
                + convertFrameToSecond(this.levelView.getLevelDefinition().getRecordTime()) + "s";
        if (this.levelView.getLevelDefinition().isJustPassTimeRecord())
            message += " NEW RECORD!!!";
        message += "\n";

        winDialog.setMessage(message);
        winDialog.setPositiveButton("New level?",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((MainActivity) getContext()).showHome();
                    }
                });
        winDialog.setNegativeButton("Retry?",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((MainActivity) getContext()).openLevel(levelView
                                .getLevelDefinition());
                    }
                });
        AlertDialog theDialog = winDialog.create();
        theDialog.show();
    }

    public float getMaxAccelerate() {
        return this.maxAccelerate;
    }


    public SparseArray<PointF> getmActivePointers() {
        return mActivePointers;
    }
}
