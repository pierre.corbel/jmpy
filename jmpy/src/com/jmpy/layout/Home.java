package com.jmpy.layout;

import java.util.Vector;

import com.example.jmpy.R;
import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;

public class Home extends View {

	private static final int NB_LEVEL_PER_LINE = 6;
	private int BUTTON_SPACING = 20;
	private int BUTTON_WIDTH;
	private int BUTTON_HEIGHT;
	private int MENU_TOP = 270;
	private Bitmap logo;
	private Bitmap available_level;
	private Bitmap disabled_level;
	private Vector<Rect> buttonHitBoxs;
	private float resolutionZoom;
	private Bitmap star;
	


	public Home(MainActivity context) {
		super(context);
		logo = BitmapFactory.decodeResource(getResources(), R.drawable.home);
		available_level = BitmapFactory.decodeResource(getResources(), R.drawable.available_level);
		disabled_level = BitmapFactory.decodeResource(getResources(), R.drawable.disabled_level);
		star = BitmapFactory.decodeResource(getResources(), R.drawable.star);
		BUTTON_WIDTH = disabled_level.getWidth();
		BUTTON_HEIGHT = disabled_level.getHeight();
		this.resolutionZoom = getResources().getDisplayMetrics().density;
		BUTTON_SPACING *= resolutionZoom;
		MENU_TOP *= resolutionZoom;
		
		setOnTouchListener(new OnHomeTouch());
	}


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = logo.getHeight();
        setMeasuredDimension(width, height);
    }
	
	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);
		Paint paint = new Paint();		
		canvas.drawARGB(255, 95, 81, 68);
		canvas.drawBitmap(logo, canvas.getWidth()/2-logo.getWidth()/2, 0, null);
		Vector<LevelDefinition> levels = ((MainActivity)getContext()).getLevels();
		buttonHitBoxs = new Vector<Rect>();
		float resolutionZoom = getResources().getDisplayMetrics().density;
		
		for (int i=0;i<levels.size(); i++) {
			LevelDefinition level = levels.get(i);

			Bitmap background = null;
			if (level.isAvailable())
				background = available_level;
			else
				background = disabled_level;
			
			int top = MENU_TOP + ((i)/NB_LEVEL_PER_LINE)*(BUTTON_HEIGHT+BUTTON_SPACING);
			int menu_width = ((NB_LEVEL_PER_LINE-1) * (BUTTON_WIDTH+BUTTON_SPACING) + BUTTON_WIDTH);
			int left = getWidth()/2 - menu_width/2 + (i)%NB_LEVEL_PER_LINE*(BUTTON_WIDTH+BUTTON_SPACING);
			canvas.drawBitmap(background, left, top, paint);
			Rect hitBox = new Rect(left, top, left+BUTTON_WIDTH, top+BUTTON_HEIGHT);
			buttonHitBoxs.add(hitBox);
			

			paint.reset();
			paint = new Paint(Paint.ANTI_ALIAS_FLAG);
			if (level.isAvailable()) {
				paint.setColor(Color.BLACK);
			} else {
				paint.setARGB(255, 182, 182, 182);
			}
			paint.setTextSize(12*resolutionZoom);
			paint.setStyle(Style.FILL);
			paint.setTextAlign(Align.CENTER);
			canvas.drawText(level.getName(), left+BUTTON_WIDTH/2, top+20*resolutionZoom, paint);
			if(level.isAvailable() && level.getMaxTime()>0 && level.getRecordTime()>0) {
				if (level.getRecordTime()<=level.getMaxTime()) {
					canvas.drawBitmap(star, left+BUTTON_WIDTH/2 - 3*resolutionZoom- star.getWidth()*3/2, top+30*resolutionZoom, paint);
				}
				if (level.getRecordTime()<=level.getSilverTime()) {
					canvas.drawBitmap(star, left+BUTTON_WIDTH/2 + 3*resolutionZoom + star.getWidth()/2, top+30*resolutionZoom, paint);
				}
				if (level.getRecordTime()<=level.getGoldTime()) {
					canvas.drawBitmap(star, left+BUTTON_WIDTH/2 - star.getWidth()/2, top+30*resolutionZoom, paint);
				}
			}
			else if(level.isAvailable() && level.getNbJumpMax()>0 && level.getRecordJump()>0) {
				if (level.getNbJumpMax()>=level.getRecordJump()) {
					canvas.drawBitmap(star, left+BUTTON_WIDTH/2 - 3*resolutionZoom- star.getWidth()*3/2, top+30*resolutionZoom, paint);
					canvas.drawBitmap(star, left+BUTTON_WIDTH/2 + 3*resolutionZoom + star.getWidth()/2, top+30*resolutionZoom, paint);
					canvas.drawBitmap(star, left+BUTTON_WIDTH/2 - star.getWidth()/2, top+30*resolutionZoom, paint);
				}
			}
		}
		
	}
	
	public class OnHomeTouch implements OnTouchListener {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				Vector<LevelDefinition> levels = ((MainActivity)getContext()).getLevels();
				for (int i=0; i<buttonHitBoxs.size();i++) {
					Rect buttonHitBox = buttonHitBoxs.get(i);
					if (levels.get(i).isAvailable() && buttonHitBox.contains((int)event.getX(), (int)event.getY())) {
						((MainActivity)getContext()).openLevel(levels.get(i));
						break;
					}
				}
			}
			return true;
		}
		
	}
}
