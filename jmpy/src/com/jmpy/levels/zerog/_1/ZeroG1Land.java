package com.jmpy.levels.zerog._1;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.LevelView;

public class ZeroG1Land extends ALand {

	public ZeroG1Land(LevelView camera) {
		super(camera);
		  
	    hitBox.addRect(new RectF(3, 3, 25, 366), Direction.CCW);

	    hitBox.addRect(new RectF(-0, 0, 2632, 26), Direction.CCW);

	    hitBox.addRect(new RectF(405, 1294, 416, 1370), Direction.CCW);

	    hitBox.addRect(new RectF(1235, -614, 1248, -316), Direction.CCW);

	    hitBox.addRect(new RectF(2607, 6, 2636, 367), Direction.CCW);

	    hitBox.addRect(new RectF(4, 318, 2632, 368), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-307, 1173, -283, 1191), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-212, 1194, -185, 1214), Direction.CCW);

	    dieHitBox.addRect(new RectF(-260, 1182, -238, 1200), Direction.CCW);

	    flagHitbox.addCircle(-227,1263, 15, Direction.CCW);

	    hitBox.addRect(new RectF(204, 890, 214, 1039), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-157, 1211, -130, 1231), Direction.CCW);

	    //startPoint.addCircle(-289,1247, 15, Direction.CCW);

	    dieHitBox.addCircle(1532,112, 44, Direction.CCW);

	    //startPoint.addCircle(2441,93, 15, Direction.CCW);

	    flagHitbox.addCircle(59,275, 15, Direction.CCW);

	    dieHitBox.addCircle(2359,65, 44, Direction.CCW);

	    dieHitBox.addRect(new RectF(1796, 249, 2163, 321), Direction.CCW);

	    dieHitBox.addRect(new RectF(1539, 117, 1807, 333), Direction.CCW);

	    dieHitBox.addRect(new RectF(10, 5, 1367, 111), Direction.CCW);

	    dieHitBox.addRect(new RectF(20, 55, 1063, 155), Direction.CCW);

	    dieHitBox.addRect(new RectF(10, 111, 527, 211), Direction.CCW);

	    dieHitBox.addCircle(2517,127, 44, Direction.CCW);

	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(true, canvas);
	}

}
