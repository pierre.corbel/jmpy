package com.jmpy.levels.zerog._1;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.zerog.ZeroGBall;
import com.jmpy.levels.zerog.ZeroGView;

public class ZeroG1View extends ZeroGView {

	public ZeroG1View(MainActivity mainActivity,
			LevelDefinition<LevelView> levelDefinition) {
		super(mainActivity, levelDefinition);
		
	}

	@Override
	protected void init() {
		super.init(new ZeroGBall(this, new Point(2441,93)), 
				new ZeroG1Land(this));
	}

}
