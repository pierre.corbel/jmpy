package com.jmpy.levels.zerog;

import android.graphics.Point;

import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;

public class ZeroGBall extends Ball {

	public ZeroGBall(LevelView levelView, Point center) {
		super(levelView, center, 1, new Vector2D(1, 1));
	}
	
	
}
