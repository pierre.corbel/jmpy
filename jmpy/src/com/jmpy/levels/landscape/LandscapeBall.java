package com.jmpy.levels.landscape;

import android.graphics.Point;

import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;

public class LandscapeBall extends Ball {

	public LandscapeBall(LevelView levelView, Point center) {
		super(levelView, center, 30, new Vector2D(0.5,0.5));
	}

}
