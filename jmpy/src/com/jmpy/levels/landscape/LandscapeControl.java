package com.jmpy.levels.landscape;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Rect;

import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;

public class LandscapeControl extends Controls {

	private static final int JUMP_CENTER_MARGIN = 10;
	
	private Vector2D controlCenter;

	public LandscapeControl(Context context, LevelView camera) {
		super(context, camera, 6, 75);
	}

	

	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		defineJumpHitBox();
		drawJumpCircle(canvas);
	}


	private void defineJumpHitBox () {
		if(controlCenter==null) {
			controlCenter = new Vector2D(levelView.getCanvasCenterOnScreen().x, levelView.getCanvasCenterOnScreen().x);
			jumpCenterHitBox = new PointF(
					(float) controlCenter.x,
					(float) controlCenter.y);
			jumpHitBox = new Rect((int) (jumpCenterHitBox.x - (maxDistance + JUMP_CENTER_MARGIN)*this.zoomResolution),
					0,
					(int) (jumpCenterHitBox.x +  (maxDistance + JUMP_CENTER_MARGIN)*this.zoomResolution),
					getHeight());
		}
	}
	
	protected void drawJumpCircle(Canvas canvas) {
		// draw jump zone
		paint.reset();
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setAlpha(150);
		canvas.drawCircle(jumpCenterHitBox.x, jumpCenterHitBox.y, maxDistance, paint);

		
		// draw jump vector
		if (previousJumpControlVector != null) {
			canvas.drawLine(
					(float) controlCenter.x,
					(float) controlCenter.y,
					(float) (controlCenter.x + previousJumpControlVector.x
							* maxDistance / maxAccelerate),
					(float) (controlCenter.y + previousJumpControlVector.y
							* maxDistance / maxAccelerate), paint);
		}
	}
}
