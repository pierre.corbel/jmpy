package com.jmpy.levels.landscape;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.ASprite;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.levels.data.LevelDefinition;

public abstract class LandscapeView extends LevelView {

	public LandscapeView(MainActivity mainActivity, LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}
	
	protected void init(Ball ball, ALand land, Controls controls) {
		super.init(ball, land, controls, (float)0.1);
	}

}
