package com.jmpy.levels.landscape._1;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.layout.Controls;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.landscape.LandscapeBall;
import com.jmpy.levels.landscape.LandscapeControl;
import com.jmpy.levels.landscape.LandscapeView;

public class Landscape1View extends LandscapeView {

	public Landscape1View(MainActivity mainActivity, LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}

	@Override
	protected void init() {
		super.init(new LandscapeBall(this, new Point(90, 478)), 
				new Land(this), 
				new LandscapeControl(getContext(), this));
	}
	
	
}
