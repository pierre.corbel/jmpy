package com.jmpy.levels.landscape._1;

import com.example.jmpy.R;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.ASprite;
import com.jmpy.engine.component.LevelView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.view.View;


public class Land extends ALand {

	private Bitmap floorBitmap;

	public Land(LevelView levelView) {
		super(levelView);
		// left
		hitBox.addRect(new RectF(0, 0, 28, 615), Direction.CCW);
		//right
		hitBox.addRect(new RectF(692, 0, 736, 597), Direction.CCW);
		// right 2
		hitBox.addRect(new RectF(677, 407, 714, 557), Direction.CCW);
		//floor 1
		hitBox.addRect(new RectF(24, 499, 325, 547), Direction.CCW);
		//floor 2
		hitBox.addRect(new RectF(384, 501, 731, 547), Direction.CCW);
		//top
		hitBox.addRect(new RectF(0, 0, 823, 47), Direction.CCW);
		
		
		//castle
		hitBox.addRect(new RectF(214, 430, 238, 501), Direction.CCW);
		hitBox.addRect(new RectF(849, 1705, 943, 2067), Direction.CCW);

		hitBox.moveTo(323, 502);
		hitBox.lineTo(342, 519);
		hitBox.lineTo(364, 520);
		hitBox.lineTo(385, 505);
		hitBox.lineTo(385, 532);
		hitBox.lineTo(323, 532);
		hitBox.lineTo(323, 502);
		
		floorBitmap = BitmapFactory.decodeResource(levelView.getResources(), R.drawable.level1);
		updateZeroG();
		flagHitbox.addRect(669, 384, 701, 416, Direction.CCW);
	}
	
	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		canvas.drawBitmap(this.floorBitmap, 0, 0, null);
		super.draw(showHitBox, canvas);
	}

}