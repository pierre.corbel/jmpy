package com.jmpy.levels.data;

public enum LevelType {
	CLASSIC,
	TIMEATTACK,
	PLATEFORM,
	CART,
	ZEROG;
}
