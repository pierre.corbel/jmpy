package com.jmpy.levels.data;

import java.sql.SQLInput;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.util.Log;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.data.db.LevelDbHelper;
import com.jmpy.levels.data.db.LevelSchema;

public class LevelDefinition<T extends LevelView> {
	private String Name;
	private LevelType type;
	private Class<T> content;
	private int nbJumpMax;
	private int maxTime;
	private int index;
	private int recordJump=0;
	private int recordTime=0;
	private boolean available;
	private MainActivity context;
	private int dbID = 0;
	private boolean justPassTimeRecord;
	private boolean justPassJumpRecord;
	private int nextLevelIndex;
	private int bonusLevelIndex;
	private Bitmap hint;
	private int goldTime;
	
	public LevelDefinition(MainActivity context,int index, String name, Class<T> content, int nbJumpMax, int maxTime, int goldTime, int nextLevelIndex, int bonusLevelIndex, Bitmap hint) {
		super();
		Name = name;
		this.type = type;
		this.content = content;
		this.hint = hint;
		
		this.nbJumpMax = nbJumpMax;
		this.maxTime = maxTime;
		this.index = index;		
		this.context = context;
		this.nextLevelIndex = nextLevelIndex;
		this.bonusLevelIndex = bonusLevelIndex;
		this.goldTime = goldTime;
		try {
			LevelDbHelper levelDbHelper = new LevelDbHelper(context);
			SQLiteDatabase db = levelDbHelper.getReadableDatabase();
			try {
				String[] columns = {LevelSchema._ID,LevelSchema.COLUMN_JUMP_RECORD,LevelSchema.COLUMN_TIME_RECORD,LevelSchema.AVAILABLE};
				String[] selectionArgs = {String.valueOf(index)};
				Cursor cursor = db.query(LevelSchema.TABLE_NAME, columns, LevelSchema.COLUMN_KEY+ " = ?", selectionArgs, null, null, null);
				try {
					if (cursor.getCount()>0) {
						cursor.moveToFirst();
						this.dbID = cursor.getInt(cursor.getColumnIndexOrThrow(LevelSchema._ID));
						this.recordJump = cursor.getInt(cursor.getColumnIndexOrThrow(LevelSchema.COLUMN_JUMP_RECORD));
						this.recordTime = cursor.getInt(cursor.getColumnIndexOrThrow(LevelSchema.COLUMN_TIME_RECORD));
						this.setAvailable(cursor.getInt(cursor.getColumnIndexOrThrow(LevelSchema.AVAILABLE))!=0);
					} else {
						ContentValues values = new ContentValues();
						values.put(LevelSchema.COLUMN_KEY, this.index);
						dbID = (int) db.insert(
								LevelSchema.TABLE_NAME,
								null,
						        values);
						this.recordJump = 0;
						this.recordTime = 0;
						this.available = false;
					}
				} finally {
					cursor.close();
				}
			} finally {
				db.close();
			}
			// First level is always available
			if (index==0) {
				setAvailable(true);
			}
		} catch (SQLiteException e) {
			Log.d("DATA", "data certainly not ready yet", e);
		}
	}
	
	public String getName() {
		return Name;
	}
	
	public LevelType getType() {
		return type;
	}
	public Class<T> getContent() {
		return content;
	}

	public int getNbJumpMax() {
		return nbJumpMax;
	}

	public int getMaxTime() {
		return maxTime;
	}

	public int getGoldTime() {
		return goldTime;
	}

	public int getSilverTime() {
		return goldTime + (maxTime-goldTime)/3;
	}
	
	public int getRecordJump() {
		return recordJump;
	}

	public int getRecordTime() {
		return recordTime;
	}
	
	public Bitmap getHint() {
		return hint;
	}

	public int getIndex() {
		return index;
	}

	public boolean finishLevel(int nbJump, int time) {
		storeJumpRecord(nbJump);
		storeTimeRecord(time);
		if((maxTime == 0 || maxTime >= time) && (nbJumpMax == 0 || nbJumpMax >= nbJump)) {
			unblockLevel(this.nextLevelIndex);
			return true;
		} else {
			return false;
		}
	}

	public boolean finishBonusLevel(int nbJump, int time) {
		storeJumpRecord(nbJump);
		storeTimeRecord(time);
		unblockLevel(this.bonusLevelIndex);
		return true;
	}
	
	private void unblockLevel(int levelIndex) {
		if (levelIndex == 0)
			return ;
		LevelDbHelper levelDbHelper = new LevelDbHelper(context);
		SQLiteDatabase db = levelDbHelper.getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put(LevelSchema.AVAILABLE, 1);
			if (dbID==0) {
				dbID = (int) db.insert(
						LevelSchema.TABLE_NAME,
						null,
				        values);
			} else {
				String[] whereArgs = {String.valueOf(levelIndex)};
				db.update(LevelSchema.TABLE_NAME, values, LevelSchema.COLUMN_KEY+" = ?", whereArgs);
			}
			context.getLevels().get(levelIndex).setAvailable(true);
		} finally {
			db.close();
		}
	}

	private void storeTimeRecord(int time) {
		if (time<this.recordTime || this.recordTime ==0) {
			this.recordTime = time;
			this.justPassTimeRecord = true;
			LevelDbHelper levelDbHelper = new LevelDbHelper(context);
			SQLiteDatabase db = levelDbHelper.getWritableDatabase();
			try {
				ContentValues values = new ContentValues();
				values.put(LevelSchema.COLUMN_KEY, this.index);
				values.put(LevelSchema.COLUMN_TIME_RECORD, time);
				if (dbID==0) {
					dbID = (int) db.insert(
							LevelSchema.TABLE_NAME,
							null,
					        values);
				} else {
					String[] whereArgs = {String.valueOf(dbID)};
					db.update(LevelSchema.TABLE_NAME, values, LevelSchema._ID+" = ?", whereArgs);
				}
			} finally {
				db.close();
			}
		} else {
			this.justPassTimeRecord = false;
		}
	}

	private void storeJumpRecord(int nbJump) {
		if (nbJump<this.recordJump || this.recordJump ==0) {
			this.recordJump = nbJump;
			this.justPassJumpRecord = true;
			LevelDbHelper levelDbHelper = new LevelDbHelper(context);
			SQLiteDatabase db = levelDbHelper.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(LevelSchema.COLUMN_KEY, this.index);
			values.put(LevelSchema.AVAILABLE, 1);
			values.put(LevelSchema.COLUMN_JUMP_RECORD, nbJump);
			if (dbID==0) {
				dbID = (int) db.insert(
						LevelSchema.TABLE_NAME,
						null,
				        values);
			} else {
				String[] whereArgs = {String.valueOf(dbID)};
				db.update(LevelSchema.TABLE_NAME, values, LevelSchema._ID+" = ?", whereArgs);
			}
			db.close();
		} else {
			this.justPassJumpRecord = false;
		}
	}
	public boolean isJustPassTimeRecord() {
		return justPassTimeRecord;
	}

	public boolean isJustPassJumpRecord() {
		return justPassJumpRecord;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	
}
