package com.jmpy.levels.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class LevelDbHelper extends SQLiteOpenHelper {

	public LevelDbHelper(Context context) {
		super(context, "levels.db", null, 3);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(LevelSchema.SQL_CREATE_LEVEL_STORAGE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(LevelSchema.SQL_DELETE_LEVEL_STORAGE);
        onCreate(db);
	}

}
