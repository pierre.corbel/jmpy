package com.jmpy.levels.data.db;

import android.provider.BaseColumns;

public class LevelSchema implements BaseColumns {
	
	public static final String TABLE_NAME = "Level";
    public static final String COLUMN_KEY = "key";
    public static final String AVAILABLE = "available";
    public static final String COLUMN_JUMP_RECORD = "jump_record";
    public static final String COLUMN_TIME_RECORD = "time_record";

    protected static final String SQL_CREATE_LEVEL_STORAGE = "CREATE TABLE "+TABLE_NAME+" ("+
    		_ID+"  INTEGER PRIMARY KEY,"+
    		COLUMN_KEY+" INTEGER,"+
    		AVAILABLE+" INTEGER,"+
    		COLUMN_JUMP_RECORD+" INTEGER,"+
    		COLUMN_TIME_RECORD+" INTEGER"+
    		")";
    
    protected static final String SQL_DELETE_LEVEL_STORAGE = "DROP TABLE IF EXISTS " + TABLE_NAME;
}