package com.jmpy.levels.jetpack._1;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.LevelView;

public class JetPack1Land extends ALand {

	public JetPack1Land(LevelView camera) {
		super(camera);
		  
		  

	    hitBox.addRect(new RectF(0, 6, 22, 1032), Direction.CCW);

	    hitBox.addRect(new RectF(3111, 62, 3125, 394), Direction.CCW);

	    hitBox.addRect(new RectF(0, 1016, 2269, 1036), Direction.CCW);

	    //startPoint.addRect(new RectF(29, 911, 59, 941), Direction.CCW);

	    hitBox.addRect(new RectF(-293, 633, -273, 782), Direction.CCW);

	    hitBox.addRect(new RectF(-651, 739, -632, 805), Direction.CCW);

	    hitBox.addRect(new RectF(5, 946, 194, 1020), Direction.CCW);

	    hitBox.addRect(new RectF(2267, -33, 2289, 1007), Direction.CCW);

	    hitBox.addRect(new RectF(-740, 652, -445, 664), Direction.CCW);

	    hitBox.addRect(new RectF(-497, 838, -77, 850), Direction.CCW);

	    hitBox.addRect(new RectF(1085, 962, 1385, 1024), Direction.CCW);

	    dieHitBox.addCircle(-320,403, 38, Direction.CCW);

	    dieHitBox.addRect(new RectF(188, 971, 1103, 1033), Direction.CCW);

	    dieHitBox.addRect(new RectF(1378, 964, 2050, 1026), Direction.CCW);

	    dieHitBox.addCircle(823,915, 38, Direction.CCW);

	    dieHitBox.addCircle(468,876, 38, Direction.CCW);

	    dieHitBox.addCircle(600,657, 38, Direction.CCW);

	    flagHitbox.addRect(new RectF(59, 57, 81, 83), Direction.CCW);

	    hitBox.addRect(new RectF(0, 509, 1597, 529), Direction.CCW);

	    hitBox.addRect(new RectF(2029, 962, 2259, 1024), Direction.CCW);

	    dieHitBox.addCircle(1272,367, 38, Direction.CCW);

	    dieHitBox.addCircle(1264,175, 38, Direction.CCW);

	    dieHitBox.addCircle(1498,327, 38, Direction.CCW);

	    dieHitBox.addCircle(1490,135, 38, Direction.CCW);

	    dieHitBox.addCircle(1693,266, 38, Direction.CCW);

	    dieHitBox.addCircle(1685,74, 38, Direction.CCW);

	    dieHitBox.addCircle(1755,757, 38, Direction.CCW);

	    dieHitBox.addCircle(1915,394, 38, Direction.CCW);

	    dieHitBox.addCircle(1592,789, 38, Direction.CCW);

	    dieHitBox.addCircle(1895,622, 38, Direction.CCW);

	    dieHitBox.addCircle(864,732, 38, Direction.CCW);

	    dieHitBox.addRect(new RectF(1108, 527, 1245, 803), Direction.CCW);

	    hitBox.addRect(new RectF(2, 0, 2275, 20), Direction.CCW);

	    dieHitBox.addRect(new RectF(900, 356, 1037, 520), Direction.CCW);

	    dieHitBox.addRect(new RectF(418, 14, 555, 322), Direction.CCW);

	    dieHitBox.addRect(new RectF(18, 492, 1601, 512), Direction.CCW);

	    dieHitBox.addCircle(252,659, 38, Direction.CCW);


	    updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(true, canvas);
	}

}
