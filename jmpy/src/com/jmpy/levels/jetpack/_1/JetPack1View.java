package com.jmpy.levels.jetpack._1;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.levels.cart.CartBall;
import com.jmpy.levels.cart.CartView;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.jetpack.JetPackBall;
import com.jmpy.levels.jetpack.JetPackView;
import com.jmpy.levels.zerog.ZeroGBall;
import com.jmpy.levels.zerog.ZeroGView;

public class JetPack1View extends JetPackView {

	public JetPack1View(MainActivity mainActivity,
			LevelDefinition<LevelView> levelDefinition) {
		super(mainActivity, levelDefinition);
		
	}

	@Override
	protected void init() {
		super.init(new JetPackBall(this, new Point(31,901)), 
				new JetPack1Land(this));
	}

}
