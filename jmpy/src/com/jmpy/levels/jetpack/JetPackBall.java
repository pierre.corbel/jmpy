package com.jmpy.levels.jetpack;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;

import com.example.jmpy.R;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;

public class JetPackBall extends Ball {
	private static final int NB_FRAME_WAIT = 6;
	private Bitmap[] jumpRight = new Bitmap[2];
	private Bitmap[] jumpLeft = new Bitmap[2];
	private Bitmap[] walkRight = new Bitmap[4];
	private Bitmap[] walkLeft = new Bitmap[4];
	private Bitmap[] fallRight = new Bitmap[2];
	private Bitmap[] fallLeft = new Bitmap[2];
	private Bitmap[] waitRight = new Bitmap[1];
	private Bitmap[] waitLeft = new Bitmap[1];
	private int currentFrame = 0;
	private boolean previousMoveIsRight = true;

	public JetPackBall(LevelView levelView, Point center) {
		super(levelView, center, 12, new Vector2D(1, 0));
		hitBoxRadius = 15;
        this.accelerate(new Vector2D(0, 0));
        walkRight[0]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.run1_jetpack);
        walkRight[1]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.run2_jetpack);
        walkRight[2]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.run3_jetpack);
        walkRight[3]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.run2_jetpack);
        waitRight[0]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.run2_jetpack);
        jumpRight[0]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.jump1_jetpack);
        jumpRight[1]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.jump2_jetpack);
        //jumpRight[2]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.fly3);
		fallRight[0]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.fall1_jetpack);
		fallRight[1]=BitmapFactory.decodeResource(levelView.getResources(), R.drawable.fall2_jetpack);
		Matrix reverseMatrix = new Matrix();
		reverseMatrix.preScale(-1, 1);
		walkLeft[0]=Bitmap.createBitmap(walkRight[0], 0, 0, walkRight[0].getWidth(), walkRight[0].getHeight(), reverseMatrix, false);
		walkLeft[1]=Bitmap.createBitmap(walkRight[1], 0, 0, walkRight[1].getWidth(), walkRight[1].getHeight(), reverseMatrix, false);
		walkLeft[2]=Bitmap.createBitmap(walkRight[2], 0, 0, walkRight[2].getWidth(), walkRight[2].getHeight(), reverseMatrix, false);
		walkLeft[3]=Bitmap.createBitmap(walkRight[3], 0, 0, walkRight[2].getWidth(), walkRight[3].getHeight(), reverseMatrix, false);
		waitLeft[0]=Bitmap.createBitmap(waitRight[0], 0, 0, waitRight[0].getWidth(), waitRight[0].getHeight(), reverseMatrix, false);
		jumpLeft[0]=Bitmap.createBitmap(jumpRight[0], 0, 0, jumpRight[0].getWidth(), jumpRight[0].getHeight(), reverseMatrix, false);
		jumpLeft[1]=Bitmap.createBitmap(jumpRight[1], 0, 0, jumpRight[1].getWidth(), jumpRight[1].getHeight(), reverseMatrix, false);
		//jumpLeft[2]=Bitmap.createBitmap(jumpRight[2], 0, 0, jumpRight[2].getWidth(), jumpRight[2].getHeight(), reverseMatrix, false);
		fallLeft[0]=Bitmap.createBitmap(fallRight[0], 0, 0, fallRight[0].getWidth(), fallRight[0].getHeight(), reverseMatrix, false);
		fallLeft[1]=Bitmap.createBitmap(fallRight[1], 0, 0, fallRight[1].getWidth(), fallRight[1].getHeight(), reverseMatrix, false);
	}


	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		currentFrame++;
		//super.draw(canvas, true);
		Bitmap[] animmatedBitmap = getAnimatedBitmaps();

			
		int nbFrame = getNbFrameToWait(animmatedBitmap);
		int key = Math.round(currentFrame/nbFrame)%animmatedBitmap.length;
		Bitmap currentAnim = animmatedBitmap[key];
		if(this.levelView.getReverseGravity()!=null) {
			Matrix upsideDownMatrix = new Matrix();
			upsideDownMatrix.setScale(1, -1);
			currentAnim = Bitmap.createBitmap(currentAnim, 0, 0, currentAnim.getWidth(), currentAnim.getHeight(), upsideDownMatrix, false);
		}
		canvas.drawBitmap(currentAnim, (float)getCenter().x-currentAnim.getWidth()/2, (float)getCenter().y-currentAnim.getHeight()/2, null);
		
	}


	private int getNbFrameToWait(Bitmap[] animmatedBitmap) {
		int nbFrame = NB_FRAME_WAIT;
		if ((animmatedBitmap==walkLeft||animmatedBitmap==walkRight) && this.getMaxSpeed()*2/3 <this.getSpeed().getNorm())
			nbFrame*=0.7;
		if ((animmatedBitmap==walkLeft||animmatedBitmap==walkRight) && this.getMaxSpeed()*1/3 >this.getSpeed().getNorm())
			nbFrame*=1.3;
		return nbFrame;
	}


	private Bitmap[] getAnimatedBitmaps() {
		if(this.getSpeed().getNorm()<1) {
			return previousMoveIsRight?waitRight:waitLeft;
		}
		if(this.getSpeed().isFullRight()) {
			previousMoveIsRight = true;
			if(this.getSpeed().isFullTop() || (this.levelView.getReverseGravity()!=null && this.getSpeed().isFullBottom()))
				return this.jumpRight;
			else if (this.getSpeed().isFullBottom() || (this.levelView.getReverseGravity()!=null && this.getSpeed().isFullTop()))
				return this.fallRight;
			return walkRight;
			
		} else if(this.getSpeed().isFullLeft()) {
			previousMoveIsRight = false;
			if(this.getSpeed().isFullTop() || (this.levelView.getReverseGravity()!=null && this.getSpeed().isFullBottom()))
				return this.jumpLeft;
			else if (this.getSpeed().isFullBottom() || (this.levelView.getReverseGravity()!=null && this.getSpeed().isFullTop()))
				return this.fallLeft;
			return walkLeft;
		}
		return walkLeft;
	}
	

	public void accelerate (Vector2D acceleration) {
		if(acceleration==null)
			return;
		acceleration.increase(speed);
		if (speed.x > this.maxSpeed)
			speed.x = this.maxSpeed;
		if (speed.x < -this.maxSpeed)
			speed.x = -this.maxSpeed;
		
		if (speed.y > this.maxSpeed)
			speed.y = this.maxSpeed;
		if (speed.y < -this.maxSpeed)
			speed.y = -this.maxSpeed;
		
	}
}
