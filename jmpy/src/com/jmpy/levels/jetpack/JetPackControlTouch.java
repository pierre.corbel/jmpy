package com.jmpy.levels.jetpack;

import android.graphics.PointF;
import android.media.audiofx.AudioEffect.OnControlStatusChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.layout.OnControlTouch;
import com.jmpy.layout.OnControlTouch.ACCELERATION_TYPE;

public class JetPackControlTouch extends OnControlTouch {

	public enum ACCELERATION_TYPE {
		LEFT,
		RIGHT,
		JUMP
	};
	@Override
	protected void clearPosition(PointF point, Controls controlView) {
		JetPackControls jetPackControlView = (JetPackControls) controlView;
		if(jetPackControlView.getLeftHitBox().contains((int)point.x, (int)point.y)) {
			jetPackControlView.clearFuturPushAcceleration(ACCELERATION_TYPE.LEFT);
		} else if (jetPackControlView.getRightHitBox().contains((int)point.x, (int)point.y)) {
			jetPackControlView.clearFuturPushAcceleration(ACCELERATION_TYPE.RIGHT);
		}  else if (jetPackControlView.getUpHitBox().contains((int)point.x, (int)point.y)) {
			jetPackControlView.clearFuturPushAcceleration(ACCELERATION_TYPE.JUMP);
		} 
	}

	@Override
	protected void evaluateNewPosition(PointF point, Controls controlView, EVENT_TYPE eventType) {
		if(eventType == EVENT_TYPE.DOWN) {
			JetPackControls plateformControlView = (JetPackControls) controlView;
			if(plateformControlView.getLeftHitBox().contains((int)point.x, (int)point.y)) {
				Vector2D clickVector = new Vector2D(plateformControlView.getMaxDistance(), 0);
				plateformControlView.calculateFuturPushAcceleration(ACCELERATION_TYPE.LEFT);
			} else if (plateformControlView.getRightHitBox().contains((int)point.x, (int)point.y)) {
				Vector2D clickVector = new Vector2D(-plateformControlView.getMaxDistance(), 0);
				plateformControlView.calculateFuturPushAcceleration(ACCELERATION_TYPE.RIGHT);
			}  else if (plateformControlView.getUpHitBox().contains((int)point.x, (int)point.y)) {
				plateformControlView.calculateFuturPushAcceleration(ACCELERATION_TYPE.JUMP);
			}
		}
	}

}