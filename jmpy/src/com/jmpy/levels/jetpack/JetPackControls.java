package com.jmpy.levels.jetpack;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.levels.jetpack.JetPackControlTouch.ACCELERATION_TYPE;

public class JetPackControls extends Controls {

	
	private Bitmap upButton;
	private Rect upHitBox;
	private Bitmap leftButton;
	private Bitmap rightButton;
	private Rect leftHitBox;
	private Rect rightHitBox;
	private float resolutionZoom;
	
	
	public JetPackControls(Context context, LevelView camera) {
		super(context, camera, 2, 110);
		upButton = BitmapFactory.decodeResource(getResources(), R.drawable.up);
		this.leftButton = BitmapFactory.decodeResource(getResources(), R.drawable.left);
		this.rightButton = BitmapFactory.decodeResource(getResources(), R.drawable.right);
		resolutionZoom = context.getResources().getDisplayMetrics().density;
		setOnTouchListener(new JetPackControlTouch());
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(leftHitBox==null) {
			leftHitBox = new Rect(0, getHeight() - leftButton.getHeight() - 2*BUTTON_PADDING, BUTTON_PADDING +leftButton.getWidth()+BUTTON_PADDING, getHeight());
			rightHitBox = new Rect(leftHitBox.right+1, getHeight() - rightButton.getHeight() -2*BUTTON_PADDING,leftHitBox.right + BUTTON_PADDING + rightButton.getWidth() + BUTTON_PADDING, getHeight());
			upHitBox = new Rect(getWidth() - 2*BUTTON_PADDING - upButton.getWidth(), getHeight() - upButton.getHeight() -2*BUTTON_PADDING,getWidth(), getHeight());
		}
		if(!isShowHint()) {
			canvas.drawBitmap(leftButton, leftHitBox.left +BUTTON_PADDING, leftHitBox.top + BUTTON_PADDING, null);
			canvas.drawBitmap(rightButton, rightHitBox.left +BUTTON_PADDING, rightHitBox.top +BUTTON_PADDING , null);
			canvas.drawBitmap(upButton, upHitBox.left +BUTTON_PADDING, upHitBox.top + BUTTON_PADDING, null);
		}
		
		Log.d("move", ""+moveControlVector);
	}

	public Rect getUpHitBox() {
		return upHitBox;
	}

	public void setUpHitBox(Rect upHitBox) {
		this.upHitBox = upHitBox;
	}

	public Rect getRightHitBox() {
		return rightHitBox;
	}

	public void setRightHitBox(Rect rightHitBox) {
		this.rightHitBox = rightHitBox;
	}


	public Rect getLeftHitBox() {
		return leftHitBox;
	}

	public void clearFuturPushAcceleration(ACCELERATION_TYPE move) {
		
		if (move==ACCELERATION_TYPE.LEFT) {
			moveControlVector = null;
		} else if (move==ACCELERATION_TYPE.RIGHT) {
			Log.d("move", "CLEAR");
			moveControlVector = null;
		} else if (move==ACCELERATION_TYPE.JUMP) {
			Log.d("move", "CLEAR");
			jumpControlVector = null;
		}
	}

	public void calculateFuturPushAcceleration(ACCELERATION_TYPE move) {
		if (move==ACCELERATION_TYPE.LEFT) {
			moveControlVector = new Vector2D(-1, 0);
		} else if (move==ACCELERATION_TYPE.RIGHT) {
			moveControlVector = new Vector2D(1, 0);
		} else if (move==ACCELERATION_TYPE.JUMP) {
			jumpControlVector = new Vector2D(0, -levelView.getGravity()*2);
		}
	}
}
