package com.jmpy.levels.jetpack._2;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import com.example.jmpy.R;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.LevelView;

public class JetPack2Land extends ALand {

    public JetPack2Land(LevelView camera) {
        super(camera);


        hitBox.addRect(new RectF(0, 6, 22, 1512), Direction.CCW);

        hitBox.addRect(new RectF(3111, 62, 3125, 394), Direction.CCW);

        hitBox.addRect(new RectF(0, 1496, 2269, 1516), Direction.CCW);

        //startPoint.addRect(new RectF(29, 1391, 59, 1421), Direction.CCW);

        hitBox.addRect(new RectF(-293, 633, -273, 782), Direction.CCW);

        hitBox.addRect(new RectF(-651, 739, -632, 805), Direction.CCW);

        hitBox.addRect(new RectF(5, 1426, 194, 1500), Direction.CCW);

        hitBox.addRect(new RectF(1883, 7, 1905, 1525), Direction.CCW);

        hitBox.addRect(new RectF(-740, 652, -445, 664), Direction.CCW);

        hitBox.addRect(new RectF(-497, 838, -77, 850), Direction.CCW);

        dieHitBox.addCircle(-320, 403, 38, Direction.CCW);

        dieHitBox.addCircle(77, 1143, 38, Direction.CCW);

        hitBox.addRect(new RectF(2, 0, 2275, 20), Direction.CCW);

        hitBox.addRect(new RectF(277, 333, 299, 1511), Direction.CCW);

        dieHitBox.addCircle(78, 296, 38, Direction.CCW);

        dieHitBox.addCircle(222, 726, 38, Direction.CCW);

        dieHitBox.addCircle(509, 1167, 38, Direction.CCW);

        flagHitbox.addRect(new RectF(843, 489, 865, 515), Direction.CCW);

        dieHitBox.addCircle(510, 362, 38, Direction.CCW);

        dieHitBox.addCircle(344, 756, 38, Direction.CCW);

        hitBox.addRect(new RectF(547, 20, 569, 1144), Direction.CCW);

        dieHitBox.addRect(new RectF(821, 1081, 1051, 1497), Direction.CCW);

        dieHitBox.addRect(new RectF(1369, 796, 1534, 1062), Direction.CCW);

        hitBox.addRect(new RectF(1090, 793, 1671, 881), Direction.CCW);

        hitBox.addRect(new RectF(544, 835, 1125, 923), Direction.CCW);

        hitBox.addRect(new RectF(957, 254, 981, 690), Direction.CCW);

        bounceHitBox.addRect(new RectF(307, 1487, 862, 1504), Direction.CCW);

        //bounceHitBox.addRect(new RectF(558, 272, 584, 835), Direction.CCW);

        dieHitBox.addRect(new RectF(1374, 641, 1539, 876), Direction.CCW);

        hitBox.addRect(new RectF(567, 13, 1892, 358), Direction.CCW);

        bounceHitBox.addRect(new RectF(8, 14, 563, 31), Direction.CCW);

        //bounceHitBox.addRect(new RectF(564, 827, 1119, 844), Direction.CCW);

        setFloorBitmap(BitmapFactory.decodeResource(levelView.getResources(), R.drawable.bonus1));
        top = -16;
        left = -38;

        updateZeroG();
    }

    @Override
    public void draw(Canvas canvas, boolean showHitBox) {

        super.draw(false, canvas);
    }

}
