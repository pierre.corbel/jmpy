package com.jmpy.levels.jetpack;

import android.util.Log;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.BounceData;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.levels.data.LevelDefinition;

public abstract class JetPackView extends LevelView {

	public JetPackView(MainActivity mainActivity,
			LevelDefinition<LevelView> levelDefinition) {
		super(mainActivity, levelDefinition);
	}

	protected void init(Ball ball, ALand land) {
		super.init(ball, land, new JetPackControls(getContext(), this),  (float)0.2);
	}
	
	@Override
	protected void manageAcceleration (int nbFrameSinceTouchTheFloor, BounceData wallJumpData) {
		Vector2D moveControlVector = getControls().getMoveControlVector();
		Vector2D jumpControlVector = getControls().getJumpControlVector();
		Vector2D resultMove = new Vector2D(0, 0);
		Vector2D resultJump = new Vector2D(0, 0);
		Vector2D result = new Vector2D(0, 0);
		
		if(jumpControlVector!=null) {
			resultJump = jumpControlVector;
			
			this.nbJump ++;
		}
		if (moveControlVector!=null && moveControlVector.isLeft()) {
			resultMove = new Vector2D(-0.2, 0);
		} else if (moveControlVector!=null && moveControlVector.isRight()) {
			resultMove = new Vector2D(0.2, 0);
		}
		resultJump.increase(result);
		resultMove.increase(result);
		Log.d("move 2", ""+resultMove);
		pushAccelerate = result;
		if (Math.abs(this.ball.getSpeed().x)>this.getControls().getMaxAccelerate()*2/3){
			this.ball.getSpeed().x += this.ball.getSpeed().x>0 ? -0.1 : 0.1;
		}
	}

}
