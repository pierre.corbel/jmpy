package com.jmpy.levels.cart;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.BounceData;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.levels.data.LevelDefinition;

public abstract class CartView extends LevelView {

	public CartView(MainActivity mainActivity,
			LevelDefinition<LevelView> levelDefinition) {
		super(mainActivity, levelDefinition);
		// TODO Auto-generated constructor stub
	}

	protected void init(Ball ball, ALand land, Vector2D initialDirection) {
		super.init(ball, land, new CartControls(getContext(), this, initialDirection), 0);
	}
	
	@Override
	protected void manageAcceleration (int nbFrameSinceTouchTheFloor, BounceData wallJumpData) {
		if (controls.getJumpControlVector()!=null) {
			this.nbJump  ++;
			pushAccelerate = controls.getJumpControlVector();
		}
	}
	

}
