package com.jmpy.levels.cart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.Log;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.levels.cart.OnCartControlTouch.ACCELERATION_TYPE;

public class CartControls extends Controls {

	private static final double ROTATION_LEFT_RIGHT = 0.05;
	private static final double DECCELERATION_LEFT_RIGHT = 0.015;
	private static final double DECCELERATION_BRAKE = 0.2;
	private static final double DECCELERATION_DONOTHING = 0.03;
	private static final double ACCELERATION_FRONT = 0.2;
	Vector2D initialDirection;
	private Bitmap upButton;
	private Bitmap bottomButton;
	private Rect upHitBox;
	private Rect bottomHitBox;
	private Bitmap leftButton;
	private Bitmap rightButton;
	private Rect leftHitBox;
	private Rect rightHitBox;
	private float resolutionZoom;
	private ACCELERATION_TYPE turn;
	private ACCELERATION_TYPE acceleration;
	
	public CartControls(Context context, LevelView camera, Vector2D initialDirection) {
		super(context, camera, 2, 110);
		this.initialDirection = initialDirection;
		upButton = BitmapFactory.decodeResource(getResources(), R.drawable.up);
		bottomButton = BitmapFactory.decodeResource(getResources(), R.drawable.down);
		this.leftButton = BitmapFactory.decodeResource(getResources(), R.drawable.left);
		this.rightButton = BitmapFactory.decodeResource(getResources(), R.drawable.right);
		resolutionZoom = context.getResources().getDisplayMetrics().density;
		setOnTouchListener(new OnCartControlTouch());
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(leftHitBox==null) {
			leftHitBox = new Rect(0, getHeight() - leftButton.getHeight() - 2*BUTTON_PADDING, BUTTON_PADDING +leftButton.getWidth()+BUTTON_PADDING, getHeight());
			rightHitBox = new Rect(leftHitBox.right+1, getHeight() - rightButton.getHeight() -2*BUTTON_PADDING,leftHitBox.right + BUTTON_PADDING + rightButton.getWidth() + BUTTON_PADDING, getHeight());
			bottomHitBox = new Rect(getWidth() - 2*BUTTON_PADDING - bottomButton.getWidth(), getHeight() - bottomButton.getHeight() -2*BUTTON_PADDING,getWidth(), getHeight());
			upHitBox = new Rect(bottomHitBox.left, bottomHitBox.top -upButton.getHeight()-2*BUTTON_PADDING-1, getWidth(), bottomHitBox.top-1);
		}
		if(!isShowHint()) {
			canvas.drawBitmap(leftButton, leftHitBox.left +BUTTON_PADDING, leftHitBox.top + BUTTON_PADDING, null);
			canvas.drawBitmap(rightButton, rightHitBox.left +BUTTON_PADDING, rightHitBox.top +BUTTON_PADDING , null);
			canvas.drawBitmap(upButton, upHitBox.left +BUTTON_PADDING, upHitBox.top + BUTTON_PADDING, null);
			canvas.drawBitmap(bottomButton, bottomHitBox.left +BUTTON_PADDING, bottomHitBox.top +BUTTON_PADDING , null);
		}
		manageAcceleration();
	}
	
	private void manageAcceleration() {
		Vector2D currentSpeedVector = levelView.getBall().getSpeed();
		if(currentSpeedVector==null || currentSpeedVector.getNorm() == 0)
			currentSpeedVector = this.initialDirection;
		else
			this.initialDirection = currentSpeedVector;
		
		moveControlVector = null;
		manageOneButton(currentSpeedVector, acceleration);
		if(turn!=null)
			manageOneButton(currentSpeedVector, turn);

	}

	private void manageOneButton(Vector2D currentSpeedVector, ACCELERATION_TYPE accelerationType) {
		Vector2D controlVector = null;
		
		// Launch chronometer start
		if(accelerationType!=null)
			moveControlVector = new Vector2D(0, 0);
		
		if(accelerationType == ACCELERATION_TYPE.ACCELERATE) {
			controlVector = Vector2D.createFromTethaAngleAndNorm(currentSpeedVector.getThetaAngle(), currentSpeedVector.getNorm()+ACCELERATION_FRONT);
		} else if(accelerationType == ACCELERATION_TYPE.BRAKE) {
			if(currentSpeedVector.getNorm()-DECCELERATION_BRAKE>0)
				controlVector = Vector2D.createFromTethaAngleAndNorm(currentSpeedVector.getThetaAngle(), currentSpeedVector.getNorm()-DECCELERATION_BRAKE);
		} else if(accelerationType == ACCELERATION_TYPE.TURN_LEFT) {
			double norm = 0;
			if(currentSpeedVector.getNorm()-DECCELERATION_LEFT_RIGHT>0)
				norm = DECCELERATION_LEFT_RIGHT;
			controlVector = Vector2D.createFromTethaAngleAndNorm(currentSpeedVector.getThetaAngle()-ROTATION_LEFT_RIGHT, currentSpeedVector.getNorm()-norm);
		} else if(accelerationType == ACCELERATION_TYPE.TURN_RIGHT) {
			double norm = 0;
			if(currentSpeedVector.getNorm()-DECCELERATION_LEFT_RIGHT>0)
				norm = DECCELERATION_LEFT_RIGHT;
			controlVector = Vector2D.createFromTethaAngleAndNorm(currentSpeedVector.getThetaAngle()+ROTATION_LEFT_RIGHT, currentSpeedVector.getNorm()-norm);
		} else {
			if(currentSpeedVector.getNorm()-DECCELERATION_DONOTHING>0)
				controlVector = Vector2D.createFromTethaAngleAndNorm(currentSpeedVector.getThetaAngle(), currentSpeedVector.getNorm()-DECCELERATION_DONOTHING);
		}
		
		if(controlVector==null)
			controlVector = currentSpeedVector;
		if(controlVector.getNorm()>levelView.getBall().getMaxSpeed())
			controlVector = Vector2D.createFromTethaAngleAndNorm(controlVector.getThetaAngle(), levelView.getBall().getMaxSpeed());
		this.levelView.getBall().setSpeed(controlVector);
	}

	public Rect getUpHitBox() {
		return upHitBox;
	}

	public void setUpHitBox(Rect upHitBox) {
		this.upHitBox = upHitBox;
	}

	public Rect getRightHitBox() {
		return rightHitBox;
	}

	public void setRightHitBox(Rect rightHitBox) {
		this.rightHitBox = rightHitBox;
	}

	public Vector2D getBallDirection() {
		return initialDirection;
	}

	public Rect getBottomHitBox() {
		return bottomHitBox;
	}

	public Rect getLeftHitBox() {
		return leftHitBox;
	}
	public void calculateFuturPushAcceleration(ACCELERATION_TYPE move) {
		if(move==ACCELERATION_TYPE.ACCELERATE || move==ACCELERATION_TYPE.BRAKE) 
			acceleration=move;
		else
			turn = move;
	}
	public void clearFuturPushAcceleration(ACCELERATION_TYPE move) {
		if(move==ACCELERATION_TYPE.ACCELERATE || move==ACCELERATION_TYPE.BRAKE) 
			acceleration=null;
		else
			turn = null;
	}
}
