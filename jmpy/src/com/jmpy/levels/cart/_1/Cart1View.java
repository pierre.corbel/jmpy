package com.jmpy.levels.cart._1;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.levels.cart.CartBall;
import com.jmpy.levels.cart.CartView;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.zerog.ZeroGBall;
import com.jmpy.levels.zerog.ZeroGView;

public class Cart1View extends CartView {

	public Cart1View(MainActivity mainActivity,
			LevelDefinition<LevelView> levelDefinition) {
		super(mainActivity, levelDefinition);
		
	}

	@Override
	protected void init() {
		super.init(new CartBall(this, new Point(1021,904)), 
				new Cart1Land(this), new Vector2D(1, 0));
	}

}
