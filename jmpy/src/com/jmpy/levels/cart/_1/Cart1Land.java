package com.jmpy.levels.cart._1;

import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.LevelView;

public class Cart1Land extends ALand {

	public Cart1Land(LevelView camera) {
		super(camera);
		 
	    hitBox.addRect(new RectF(1, 29, 58, 1173), Direction.CCW);

	    hitBox.addRect(new RectF(168, 1596, 179, 1672), Direction.CCW);

	    hitBox.addRect(new RectF(1537, -377, 1550, -79), Direction.CCW);

	    hitBox.addRect(new RectF(1714, 16, 1757, 1187), Direction.CCW);

	    hitBox.addRect(new RectF(46, 948, 1916, 998), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-307, 1173, -283, 1191), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-212, 1194, -185, 1214), Direction.CCW);

	    dieHitBox.addRect(new RectF(-260, 1182, -238, 1200), Direction.CCW);

	    flagHitbox.addCircle(-227,1263, 15, Direction.CCW);

	    hitBox.addRect(new RectF(851, 760, 950, 965), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-157, 1211, -130, 1231), Direction.CCW);

	    //startPoint.addCircle(-289,1247, 15, Direction.CCW);

	    //startPoint.addCircle(1021,904, 15, Direction.CCW);

	    dieHitBox.addCircle(-418,586, 44, Direction.CCW);

	    hitBox.addRect(new RectF(312, 729, 1444, 784), Direction.CCW);

	    hitBox.addRect(new RectF(904, 276, 1443, 778), Direction.CCW);

	    hitBox.addRect(new RectF(9, 507, 392, 552), Direction.CCW);

	    hitBox.addRect(new RectF(468, 276, 914, 311), Direction.CCW);

	    hitBox.addRect(new RectF(0, 0, 1889, 50), Direction.CCW);

	    hitBox.addRect(new RectF(20, 1553, 30, 1758), Direction.CCW);

	    flagHitbox.addRect(new RectF(808, 768, 855, 972), Direction.CCW);

	    hitBox.addRect(new RectF(717, 159, 1267, 290), Direction.CCW);


	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(true, canvas);
	}

}
