package com.jmpy.levels.cart;

import android.graphics.PointF;
import android.media.audiofx.AudioEffect.OnControlStatusChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.layout.OnControlTouch;
import com.jmpy.layout.OnControlTouch.ACCELERATION_TYPE;

public class OnCartControlTouch extends OnControlTouch {

	public enum ACCELERATION_TYPE {
		ACCELERATE,
		BRAKE,
		TURN_LEFT,
		TURN_RIGHT
	};

	@Override
	protected void clearPosition(PointF point, Controls controlView) {
		CartControls plateformControlView = (CartControls) controlView;
		if(plateformControlView.getLeftHitBox().contains((int)point.x, (int)point.y)) {
			plateformControlView.clearFuturPushAcceleration(ACCELERATION_TYPE.TURN_LEFT);
		} else if (plateformControlView.getRightHitBox().contains((int)point.x, (int)point.y)) {
			plateformControlView.clearFuturPushAcceleration(ACCELERATION_TYPE.TURN_RIGHT);
		}  else if (plateformControlView.getUpHitBox().contains((int)point.x, (int)point.y)) {
			plateformControlView.clearFuturPushAcceleration(ACCELERATION_TYPE.ACCELERATE);
		}  else if (plateformControlView.getBottomHitBox().contains((int)point.x, (int)point.y)) {
			plateformControlView.clearFuturPushAcceleration(ACCELERATION_TYPE.BRAKE);
		}
	}

	@Override
	protected void evaluateNewPosition(PointF point, Controls controlView, EVENT_TYPE eventType) {
		if(eventType == EVENT_TYPE.DOWN) {
			CartControls plateformControlView = (CartControls) controlView;
			if(plateformControlView.getLeftHitBox().contains((int)point.x, (int)point.y)) {
				plateformControlView.calculateFuturPushAcceleration(ACCELERATION_TYPE.TURN_LEFT);
			} else if (plateformControlView.getRightHitBox().contains((int)point.x, (int)point.y)) {
				plateformControlView.calculateFuturPushAcceleration(ACCELERATION_TYPE.TURN_RIGHT);
			}  else if (plateformControlView.getUpHitBox().contains((int)point.x, (int)point.y)) {
				plateformControlView.calculateFuturPushAcceleration(ACCELERATION_TYPE.ACCELERATE);
			}  else if (plateformControlView.getBottomHitBox().contains((int)point.x, (int)point.y)) {
				plateformControlView.calculateFuturPushAcceleration(ACCELERATION_TYPE.BRAKE);
			}
		}
	}

}