package com.jmpy.levels.cart;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;

import com.example.jmpy.R;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.BounceData;
import com.jmpy.engine.physic.Vector2D;

public class CartBall extends Ball {

	private Bitmap car;
	public CartBall(LevelView levelView, Point center) {
		super(levelView, center, 7, new Vector2D(0.2, 0.2));
		hitBoxRadius = 15;
		car = BitmapFactory.decodeResource(levelView.getResources(), R.drawable.car);
	}
	
	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		Matrix matrix= new Matrix();
		matrix.postTranslate((float)-car.getWidth()/2, (float)-car.getHeight()/2);
		matrix.postRotate((float)(Math.toDegrees(getSpeed().getThetaAngle())));
		matrix.postTranslate((float)getCenter().x, (float)getCenter().y);
		canvas.drawBitmap(car, matrix, null);
		//canvas.drawBitmap(car, (float)getCenter().x-hitBoxRadius, (float)getCenter().y-hitBoxRadius, null);
	}



	protected double getColisionAngle(Vector2D collisionDirection,
			Vector2D speedVector) {
		double newAngle = 0;
		if(speedVector.getThetaAngle()<collisionDirection.getThetaAngle())
			newAngle = collisionDirection.getThetaAngle()+4*Math.PI/9;
		else
			newAngle = collisionDirection.getThetaAngle()-4*Math.PI/9;
		return newAngle;
	}
}
