package com.jmpy.levels.plateform;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;

import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.LevelView;

public abstract class PlateformLand extends ALand {

	public PlateformLand(LevelView camera) {
		super(camera);
		updateZeroG();
	}

	@Override
	public void updateZeroG() {
		zeroGHitBox = new Path(hitBox);
		Matrix matrixFloor = new Matrix();
		matrixFloor.postTranslate(0, -5);
		zeroGHitBox.transform(matrixFloor);
	}

}
