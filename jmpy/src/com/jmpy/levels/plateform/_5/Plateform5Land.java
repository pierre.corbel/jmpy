package com.jmpy.levels.plateform._5;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Plateform5Land extends PlateformLand {


	public Plateform5Land(LevelView camera) {
		super(camera);

		  
	    hitBox.addRect(new RectF(3, 5, 25, 668), Direction.CCW);

	    hitBox.addRect(new RectF(3, 2, 2055, 28), Direction.CCW);

	    hitBox.addRect(new RectF(405, 1294, 416, 1370), Direction.CCW);

	    hitBox.addRect(new RectF(1235, -614, 1248, -316), Direction.CCW);

	    dieHitBox.addRect(new RectF(6, 620, 2456, 670), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-307, 1173, -283, 1191), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-212, 1194, -185, 1214), Direction.CCW);

	    dieHitBox.addRect(new RectF(-260, 1182, -238, 1200), Direction.CCW);

	    hitBox.addRect(new RectF(893, 1014, 903, 1163), Direction.CCW);

	    //startPoint.addCircle(1500,193, 15, Direction.CCW);

	    //startPoint.addCircle(-289,1247, 15, Direction.CCW);

	    dieHitBox.addCircle(-140,1311, 44, Direction.CCW);

	    hitBox.addRect(new RectF(1427, 5, 1477, 233), Direction.CCW);

	    dieHitBox.addCircle(1058,202, 44, Direction.CCW);

	    dieHitBox.addCircle(1176,98, 44, Direction.CCW);

	    flagHitbox.addCircle(1400,192, 15, Direction.CCW);

	    dieHitBox.addCircle(224,316, 44, Direction.CCW);

	    dieHitBox.addCircle(336,212, 44, Direction.CCW);

	    dieHitBox.addCircle(442,320, 44, Direction.CCW);

	    bonusFlagHitbox.addCircle(50,306, 15, Direction.CCW);

	    hitBox.addRect(new RectF(862, 216, 1734, 271), Direction.CCW);

	    hitBox.addRect(new RectF(1765, 514, 1934, 560), Direction.CCW);

	    hitBox.addRect(new RectF(983, 466, 1279, 512), Direction.CCW);

	    hitBox.addRect(new RectF(779, 411, 891, 457), Direction.CCW);

	    hitBox.addRect(new RectF(609, 316, 721, 362), Direction.CCW);

	    hitBox.addRect(new RectF(13, 322, 440, 368), Direction.CCW);

	    hitBox.addRect(new RectF(2015, 21, 2058, 507), Direction.CCW);

	    hitBox.addRect(new RectF(1519, 427, 1633, 473), Direction.CCW);

	    dieHitBox.addCircle(2013,198, 44, Direction.CCW);

		setFloorBitmap(BitmapFactory.decodeResource(levelView.getResources(), R.drawable.level8));

		top = 2;
		left = - 15;

    updateZeroG();

	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(false, canvas);
	}

}
