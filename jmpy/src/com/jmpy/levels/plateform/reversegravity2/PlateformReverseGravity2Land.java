	package com.jmpy.levels.plateform.reversegravity2;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;

public class PlateformReverseGravity2Land extends PlateformLand {

	public PlateformReverseGravity2Land(LevelView camera) {
		super(camera);

		  
	    //startPoint.addRect(new RectF(36, 222, 58, 248), Direction.CCW);

		//dieHitBox.addCircle(3609,72, 47, Direction.CCW);

		//dieHitBox.addCircle(3534,244, 47, Direction.CCW);

		//dieHitBox.addCircle(1137,2450, 47, Direction.CCW);

	    hitBox.addRect(new RectF(0, 20, 22, 1353), Direction.CCW);

	    dieHitBox.addRect(new RectF(0, 1338, 4457, 1358), Direction.CCW);

	    hitBox.addRect(new RectF(622, 1608, 642, 1757), Direction.CCW);

	    hitBox.addRect(new RectF(508, 1885, 527, 1951), Direction.CCW);

	    hitBox.addRect(new RectF(4452, 6, 4474, 1476), Direction.CCW);

	    hitBox.addRect(new RectF(420, 1798, 715, 1810), Direction.CCW);

	    hitBox.addRect(new RectF(413, 1490, 833, 1502), Direction.CCW);

	    hitBox.addRect(new RectF(5, 253, 284, 281), Direction.CCW);

	    hitBox.addRect(new RectF(709, 1633, 739, 1663), Direction.CCW);

	    hitBox.addRect(new RectF(18, 4, 4471, 24), Direction.CCW);

	    hitBox.addRect(new RectF(308, 1571, 327, 1703), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(1097, 1441, 1114, 2389), Direction.CCW);

	    flagHitbox.addRect(new RectF(3556, 150, 3578, 176), Direction.CCW);

	    reverseGravity.addRect(new RectF(4, 393, 972, 1104), Direction.CCW);

	    dieHitBox.addRect(new RectF(408, 6, 428, 508), Direction.CCW);

	    reverseGravity.addRect(new RectF(2392, 315, 3520, 782), Direction.CCW);

	    dieHitBox.addRect(new RectF(2399, 156, 2416, 1005), Direction.CCW);

	    dieHitBox.addRect(new RectF(716, 22, 736, 507), Direction.CCW);

	    hitBox.addRect(new RectF(1294, 264, 1483, 1076), Direction.CCW);

	    dieHitBox.addRect(new RectF(3071, 19, 3088, 389), Direction.CCW);

	    hitBox.addRect(new RectF(1219, 1040, 2123, 1165), Direction.CCW);

	    hitBox.addRect(new RectF(1761, 13, 2001, 887), Direction.CCW);

	    reverseGravity.addRect(new RectF(2005, 604, 2131, 924), Direction.CCW);

	    dieHitBox.addRect(new RectF(1468, 286, 1659, 312), Direction.CCW);

	    dieHitBox.addRect(new RectF(1596, 441, 1781, 467), Direction.CCW);

	    dieHitBox.addRect(new RectF(1460, 604, 1648, 630), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(2004, 187, 2413, 251), Direction.CCW);

	    dieHitBox.addRect(new RectF(1595, 769, 1780, 789), Direction.CCW);

	    hitBox.addRect(new RectF(1654, 758, 1913, 781), Direction.CCW);

	    hitBox.addRect(new RectF(1315, 589, 1590, 621), Direction.CCW);

	    hitBox.addRect(new RectF(1649, 431, 1930, 466), Direction.CCW);

	    getAnimatedLands().add(new DeathBlink(new RectF(562, 19, 582, 1336), camera));
	    
	    updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
