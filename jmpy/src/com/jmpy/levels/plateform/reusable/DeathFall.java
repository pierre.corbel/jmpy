package com.jmpy.levels.plateform.reusable;

import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.jmpy.engine.component.AnimatedLand;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.component.animation.Animation;
import com.jmpy.engine.component.animation.FallingAnimation;
import com.jmpy.engine.component.animation.LinearGoAndBackAnimation;

public class DeathFall extends AnimatedLand{

	public DeathFall(RectF deadZone, LevelView camera) {
		super(camera, new FallingAnimation((int) deadZone.height(), 0, 10, 30, 100, 0));
		init(deadZone);
	}

	public DeathFall(RectF deadZone, LevelView camera, int phase) {
		super(camera, new FallingAnimation((int) deadZone.height(), phase, 10, 30, 100, 0));
		init(deadZone);
	}

	public DeathFall(RectF deadZone, LevelView camera, int phase, int fallingTime, int waitingAfterFallTime, int goingUpTime, int waitingAfterGoingUpTime) {
		super(camera, new FallingAnimation((int) deadZone.height(), phase, fallingTime, waitingAfterFallTime, goingUpTime, waitingAfterGoingUpTime));
		init(deadZone);
	}

	private void init(RectF deadZone) {
		RectF upPositionRectangle = new RectF(deadZone);
		upPositionRectangle.top -= deadZone.height();
		upPositionRectangle.bottom -= deadZone.height();
		dieHitBox.addRect(upPositionRectangle, Direction.CCW);
	}

}
