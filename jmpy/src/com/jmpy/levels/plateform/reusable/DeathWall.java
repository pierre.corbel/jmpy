package com.jmpy.levels.plateform.reusable;

import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.jmpy.engine.component.AnimatedLand;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.component.animation.Animation;
import com.jmpy.engine.component.animation.LinearGoAndBackAnimation;

public class DeathWall extends AnimatedLand{

	public DeathWall(int x, int y, LevelView camera, int width) {
		super(camera, new LinearGoAndBackAnimation((int) Math.abs(width/4.8), width, 50, 0));
		init(x, y);
	}
	
	public DeathWall(int x, int y, LevelView camera, int width, int phase) {
		super(camera, new LinearGoAndBackAnimation((int) Math.abs(width/4.8), width, 50, phase));
		init(x, y);
	}
	
	public DeathWall(int x, int y, LevelView camera, int width, int phase, int waitingTime, int duration) {
		super(camera, new LinearGoAndBackAnimation(duration, width, waitingTime, phase));
		init(x, y);
	}

	private void init(int x, int y) {
		dieHitBox.addRect(new RectF(x, y, x+102, y+251), Direction.CCW);
	}

}
