package com.jmpy.levels.plateform.reusable;

import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.jmpy.engine.component.AnimatedLand;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.component.animation.Animation;
import com.jmpy.engine.component.animation.LinearGoAndBackAnimation;

public class DeathWell extends AnimatedLand{

	public DeathWell(int x, int y, LevelView camera, int width) {
		super(camera, new LinearGoAndBackAnimation((int) Math.abs((width/25)), width, 30, 0));
		init(x, y);
	}

	public DeathWell(int x, int y, LevelView camera, int width, int phase) {
		super(camera, new LinearGoAndBackAnimation((int) Math.abs((width/25)), width, 30, phase));
		init(x, y);
	}

	private void init(int x, int y) {
		dieHitBox.addCircle(x, y, 38, Direction.CCW);
	}

}
