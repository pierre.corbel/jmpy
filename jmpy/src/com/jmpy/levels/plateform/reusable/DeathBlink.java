package com.jmpy.levels.plateform.reusable;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.jmpy.engine.component.AnimatedLand;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.component.animation.Animation;
import com.jmpy.engine.component.animation.BlinkAnimation;
import com.jmpy.engine.component.animation.LinearGoAndBackAnimation;

public class DeathBlink extends AnimatedLand{

	private RectF inititialRect;
	public DeathBlink(RectF rect, LevelView camera, int duration, int phase, float ratio) {
		super(camera, new BlinkAnimation(duration, phase, ratio));
		init(rect);
	}

	private void init(RectF rect) {
		dieHitBox.addRect(rect, Direction.CCW);
		this.inititialRect = new RectF(rect);		
	}
	public DeathBlink(RectF rect, LevelView camera) {
		super(camera, new BlinkAnimation(100, 0));
		init(rect);
	}
	

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(canvas,showHitBox);
		if(showHitBox) {
			Paint paint = new Paint();
			paint.setColor(Color.RED);
			paint.setAlpha(20);
			canvas.drawRect(inititialRect, paint);
		}
	}
	

}
