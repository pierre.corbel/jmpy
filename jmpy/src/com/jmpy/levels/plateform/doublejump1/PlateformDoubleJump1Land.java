package com.jmpy.levels.plateform.doublejump1;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;
import com.jmpy.levels.plateform.reusable.DeathWell;

public class PlateformDoubleJump1Land extends PlateformLand {

	public PlateformDoubleJump1Land(LevelView camera) {
		super(camera);

		  
		  
	    hitBox.addRect(new RectF(1, 0, 23, 829), Direction.CCW);

	    hitBox.addRect(new RectF(3111, 62, 3125, 394), Direction.CCW);

	    hitBox.addRect(new RectF(2, 828, 1392, 848), Direction.CCW);

	    flagHitbox.addRect(new RectF(55, 134, 77, 160), Direction.CCW);

	    //startPoint.addRect(new RectF(31, 723, 61, 753), Direction.CCW);

	    hitBox.addRect(new RectF(-651, 739, -632, 805), Direction.CCW);

	    hitBox.addRect(new RectF(7, 758, 196, 832), Direction.CCW);

	    hitBox.addRect(new RectF(1373, 5, 1395, 847), Direction.CCW);

	    hitBox.addRect(new RectF(-740, 652, -445, 664), Direction.CCW);

	    hitBox.addRect(new RectF(-497, 838, -77, 850), Direction.CCW);

	    hitBox.addRect(new RectF(692, 766, 922, 828), Direction.CCW);

	    dieHitBox.addCircle(-320,403, 38, Direction.CCW);

	    dieHitBox.addRect(new RectF(189, 783, 705, 845), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(423, 720, 487, 779), Direction.CCW);

	    dieHitBox.addRect(new RectF(879, 782, 1395, 844), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(996, 614, 1060, 673), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(229, 212, 293, 271), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(829, 479, 893, 538), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(654, 347, 718, 406), Direction.CCW);

	    hitBox.addRect(new RectF(755, 306, 1374, 329), Direction.CCW);

	    dieHitBox.addCircle(1145,313, 38, Direction.CCW);

	    dieHitBox.addCircle(923,317, 38, Direction.CCW);

	    dieHitBox.addCircle(1043,191, 38, Direction.CCW);

	    flagHitbox.addRect(new RectF(1338, 273, 1360, 299), Direction.CCW);

	    hitBox.addRect(new RectF(762, 158, 782, 307), Direction.CCW);

	    hitBox.addRect(new RectF(0, 0, 1391, 23), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(753, 159, 773, 308), Direction.CCW);

	    hitBox.addRect(new RectF(275, 17, 295, 166), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(282, 18, 301, 167), Direction.CCW);

	    
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
