package com.jmpy.levels.plateform;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.ASprite;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.BounceData;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.zerog.ZeroGControls;

public abstract class PlateformView extends LevelView {

	private Vector2D secondAccelerate = null;
	private Vector2D moveControlVector;
	public PlateformView(MainActivity mainActivity, LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}
	
	protected void init(Ball ball, ALand land) {
		super.init(ball, land, new PlateformControl(getContext(), this), (float)0.25);
	}

	@Override
	protected void manageAcceleration (int nbFrameSinceTouchTheFloor, BounceData wallJumpData) {
		Vector2D moveControlVector = getControls().getMoveControlVector();
		Vector2D jumpControlVector = getControls().getJumpControlVector();
		Vector2D result = null;
		
		if(jumpControlVector!=null) {
			if(jumpControlVector.isFullBottom()) {
				result = jumpControlVector;
			} else if(jumpControlVector.isFullTop()) {
				result = jumpControlVector;
				this.nbJump ++;
			}
			
		} else if (moveControlVector!=null && moveControlVector.isRight() && (wallJumpData==null || wallJumpData.ColisionNormaleVector.isLeft())) {
			result = new Vector2D(nbFrameSinceTouchTheFloor<5?-0.4:-0.3, 0);
		} else if (moveControlVector!=null && moveControlVector.isLeft() && (wallJumpData==null || wallJumpData.ColisionNormaleVector.isRight())) {
			result = new Vector2D(nbFrameSinceTouchTheFloor<5?0.4:0.3, 0);
		}
		pushAccelerate = result;
		if (Math.abs(this.ball.getSpeed().x)>this.getControls().getMaxAccelerate()*2/3){
			this.ball.getSpeed().x += this.ball.getSpeed().x>0 ? -0.2 : 0.2;
		}
	}
}
