package com.jmpy.levels.plateform._4;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Plateform4Land extends PlateformLand {

	public Plateform4Land(LevelView camera) {
		super(camera);
		  
		  
	    hitBox.addRect(new RectF(3, 3, 25, 668), Direction.CCW);

	    hitBox.addRect(new RectF(20, 0, 1708, 26), Direction.CCW);

	    hitBox.addRect(new RectF(1396, 284, 1708, 341), Direction.CCW);

	    hitBox.addRect(new RectF(405, 1294, 416, 1370), Direction.CCW);

	    hitBox.addRect(new RectF(1235, -614, 1248, -316), Direction.CCW);

	    hitBox.addRect(new RectF(1684, 3, 1713, 666), Direction.CCW);

	    hitBox.addRect(new RectF(18, 618, 1712, 668), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-307, 1173, -283, 1191), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-212, 1194, -185, 1214), Direction.CCW);

	    dieHitBox.addRect(new RectF(-260, 1182, -238, 1200), Direction.CCW);

	    flagHitbox.addCircle(-227,1263, 15, Direction.CCW);

	    hitBox.addRect(new RectF(145, 1238, 155, 1387), Direction.CCW);

	    //startPoint.addCircle(100,599, 15, Direction.CCW);

	    flagHitbox.addCircle(1151,600, 15, Direction.CCW);

	    //startPoint.addCircle(-289,1247, 15, Direction.CCW);

	    dieHitBox.addCircle(-140,1311, 44, Direction.CCW);

	    hitBox.addRect(new RectF(995, 190, 1101, 635), Direction.CCW);

	    dieHitBox.addCircle(628,608, 44, Direction.CCW);

	    dieHitBox.addRect(new RectF(1100, 207, 1379, 221), Direction.CCW);

	    hitBox.addRect(new RectF(256, 452, 780, 477), Direction.CCW);

	    dieHitBox.addRect(new RectF(254, 461, 778, 486), Direction.CCW);

	    hitBox.addRect(new RectF(882, 558, 1018, 623), Direction.CCW);

	    hitBox.addRect(new RectF(160, 356, 576, 455), Direction.CCW);

	    hitBox.addRect(new RectF(10, 290, 380, 371), Direction.CCW);

	    hitBox.addRect(new RectF(466, 190, 1000, 255), Direction.CCW);

		setFloorBitmap(BitmapFactory.decodeResource(levelView.getResources(), R.drawable.level7));
		updateZeroG();
		top = -29;
		left = - 41;

	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(false, canvas);
	}

}
