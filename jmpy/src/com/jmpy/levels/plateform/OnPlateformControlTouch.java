package com.jmpy.levels.plateform;

import android.graphics.PointF;
import android.media.audiofx.AudioEffect.OnControlStatusChangeListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.jmpy.engine.component.Ball;
import com.jmpy.engine.physic.BounceData;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.layout.OnControlTouch;
import com.jmpy.layout.OnControlTouch.ACCELERATION_TYPE;

public class OnPlateformControlTouch extends OnControlTouch {


	private BounceData previousWallJumpBounceData;

	@Override
	protected void clearPosition(PointF point, Controls controlView) {
		PlateformControl plateformControlView = (PlateformControl) controlView;
		if(plateformControlView.getLeftHitBox().contains((int)point.x, (int)point.y)) {
			plateformControlView.calculateFuturPushAcceleration(null, ACCELERATION_TYPE.MOVE);
		} else if (plateformControlView.getRightHitBox().contains((int)point.x, (int)point.y)) {
			plateformControlView.calculateFuturPushAcceleration(null, ACCELERATION_TYPE.MOVE);
		} else {
			plateformControlView.calculateFuturPushAcceleration(null, ACCELERATION_TYPE.JUMP);
		}
	}

	@Override
	protected void evaluateNewPosition(PointF point, Controls controlView, EVENT_TYPE eventType) {
		if(eventType == EVENT_TYPE.DOWN) {
			PlateformControl plateformControlView = (PlateformControl) controlView;
			if(plateformControlView.getLeftHitBox().contains((int)point.x, (int)point.y)) {
				Vector2D clickVector = new Vector2D(plateformControlView.getMaxDistance(), 0);
				plateformControlView.calculateFuturPushAcceleration(clickVector, ACCELERATION_TYPE.MOVE);
			} else if (plateformControlView.getRightHitBox().contains((int)point.x, (int)point.y)) {
				Vector2D clickVector = new Vector2D(-plateformControlView.getMaxDistance(), 0);
				plateformControlView.calculateFuturPushAcceleration(clickVector, ACCELERATION_TYPE.MOVE);
			} else if (plateformControlView.getJumpHitBox().contains((int)point.x, (int)point.y)) {
				previousWallJumpBounceData = plateformControlView.getLevelView().getPreviousWallJumpBounceData();
				Ball theCharacter = plateformControlView.getLevelView().getBall();
				Vector2D clickVector = new Vector2D(plateformControlView.getControlCenter().x-point.x, plateformControlView.getControlCenter().y-point.y);
				if(clickVector.isFullBottom()) {
					clickVector = Vector2D.createFromTethaAngleAndNorm(Math.PI/2, clickVector.getNorm());
				} else if(clickVector.isSmallTop() && previousWallJumpBounceData!=null) {
					double topAngle = -Math.PI/2;
					if(previousWallJumpBounceData.ColisionNormaleVector.isRight())
						topAngle = -4*Math.PI/7;
					else if(previousWallJumpBounceData.ColisionNormaleVector.isLeft())
						topAngle = -3*Math.PI/7;
					clickVector = Vector2D.createFromTethaAngleAndNorm(topAngle, clickVector.getNorm());
				} else if (((clickVector.isFullLeft() && !theCharacter.getSpeed().isFullLeft())
							|| (clickVector.isFullRight() && !theCharacter.getSpeed().isFullRight()))
						&& Math.abs(theCharacter.getSpeed().getNorm()) > 4) {
					clickVector = Vector2D.createFromTethaAngleAndNorm(-Math.PI/2, clickVector.getNorm());
				}
				plateformControlView.calculateFuturPushAcceleration(clickVector, ACCELERATION_TYPE.JUMP);
			}
		}
	}

}