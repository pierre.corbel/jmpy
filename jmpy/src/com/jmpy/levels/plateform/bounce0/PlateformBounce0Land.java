package com.jmpy.levels.plateform.bounce0;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class PlateformBounce0Land extends PlateformLand {

	public PlateformBounce0Land(LevelView camera) {
		super(camera);
		  
		 

		  
	    hitBox.addRect(new RectF(3, 3, 25, 742), Direction.CCW);

	    hitBox.addRect(new RectF(0, 0, 2951, 26), Direction.CCW);

	    hitBox.addRect(new RectF(25, 654, 2961, 739), Direction.CCW);

	    hitBox.addRect(new RectF(584, 430, 1611, 673), Direction.CCW);

	    dieHitBox.addCircle(1227,425, 44, Direction.CCW);

	    hitBox.addRect(new RectF(2307, 433, 2949, 676), Direction.CCW);

	    dieHitBox.addCircle(2609,428, 44, Direction.CCW);

	    hitBox.addRect(new RectF(2928, 4, 2950, 743), Direction.CCW);

	    bounceHitBox.addRect(new RectF(306, 636, 607, 669), Direction.CCW);

	    bounceHitBox.addRect(new RectF(1247, 419, 1619, 452), Direction.CCW);

	    bounceHitBox.addRect(new RectF(1576, 634, 2321, 667), Direction.CCW);

	    //startPoint.addCircle(49,627, 15, Direction.CCW);

	    flagHitbox.addCircle(2888,416, 15, Direction.CCW);

	    
	    
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
