package com.jmpy.levels.plateform.bounce1;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class PlateformBounce1Land extends PlateformLand {

	public PlateformBounce1Land(LevelView camera) {
		super(camera);
		  

	    hitBox.addRect(new RectF(3, 17, 25, 732), Direction.CCW);

	    hitBox.addRect(new RectF(0, 2, 1572, 28), Direction.CCW);

	    flagHitbox.addRect(new RectF(1519, 601, 1541, 627), Direction.CCW);

	    hitBox.addRect(new RectF(216, 1148, 226, 1297), Direction.CCW);

	    hitBox.addRect(new RectF(405, 1294, 416, 1370), Direction.CCW);

	    hitBox.addRect(new RectF(1235, -614, 1248, -316), Direction.CCW);

	    hitBox.addRect(new RectF(1560, 13, 1589, 744), Direction.CCW);

	    hitBox.addRect(new RectF(1113, 393, 1600, 432), Direction.CCW);

	    hitBox.addRect(new RectF(829, 652, 907, 696), Direction.CCW);

	    hitBox.addRect(new RectF(389, 379, 541, 417), Direction.CCW);

	    hitBox.addRect(new RectF(101, 195, 133, 565), Direction.CCW);

	    hitBox.addRect(new RectF(121, 529, 421, 567), Direction.CCW);

	    hitBox.addRect(new RectF(385, 379, 423, 565), Direction.CCW);

	    hitBox.addRect(new RectF(20, 694, 1574, 744), Direction.CCW);

	    hitBox.addRect(new RectF(565, 627, 655, 703), Direction.CCW);

	    hitBox.addRect(new RectF(1094, 630, 1582, 708), Direction.CCW);

	    bounceHitBox.addRect(new RectF(827, 642, 909, 660), Direction.CCW);

	    bounceHitBox.addRect(new RectF(123, 515, 393, 533), Direction.CCW);

	    dieHitBox.addRect(new RectF(652, 684, 1136, 706), Direction.CCW);

	    dieHitBox.addRect(new RectF(544, 394, 1113, 406), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(93, 215, 107, 274), Direction.CCW);

	    dieHitBox.addRect(new RectF(712, 525, 754, 696), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(19, 211, 33, 270), Direction.CCW);

	    dieHitBox.addCircle(297,697, 45, Direction.CCW);

	    hitBox.addRect(new RectF(756, 355, 943, 399), Direction.CCW);

	    bounceHitBox.addRect(new RectF(751, 345, 948, 363), Direction.CCW);
	    
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
