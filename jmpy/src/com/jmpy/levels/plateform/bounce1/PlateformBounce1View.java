package com.jmpy.levels.plateform.bounce1;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;

public class PlateformBounce1View extends PlateformView {

	public PlateformBounce1View(MainActivity mainActivity,
			LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		super.init(new PlateformBall(this, new Point(1514, 362)), 
				new PlateformBounce1Land(this));
	}

}
