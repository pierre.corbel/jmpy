package com.jmpy.levels.plateform.tuto3;

import android.graphics.*;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Tuto3Land extends PlateformLand {

    public Tuto3Land(LevelView camera) {
        super(camera);


        hitBox.addRect(new RectF(0, 1, 22, 514), Direction.CCW);

        hitBox.addRect(new RectF(2761, 63, 2775, 395), Direction.CCW);

        hitBox.addRect(new RectF(0, 512, 1550, 532), Direction.CCW);

        //startPoint.addRect(new RectF(1471, 480, 1501, 510), Direction.CCW);

        hitBox.addRect(new RectF(-133, 474, -113, 623), Direction.CCW);

        hitBox.addRect(new RectF(-414, 519, -395, 585), Direction.CCW);

        hitBox.addRect(new RectF(1536, -65, 1558, 548), Direction.CCW);

        flagHitbox.addRect(new RectF(41, 485, 63, 511), Direction.CCW);

        hitBox.addRect(new RectF(-502, 432, -207, 444), Direction.CCW);

        dieHitBox.addRect(new RectF(230, 502, 569, 514), Direction.CCW);

        hitBox.addRect(new RectF(-583, 346, -163, 358), Direction.CCW);

        dieHitBox.addRect(new RectF(894, 502, 1114, 514), Direction.CCW);

        setFloorBitmap(BitmapFactory.decodeResource(camera.getResources(), R.drawable.tuto3));
        left = 1;
        top = 183;
        updateZeroG();
    }

    @Override
    public void draw(Canvas canvas, boolean showHitBox) {
        super.draw(false, canvas);
    }

}
