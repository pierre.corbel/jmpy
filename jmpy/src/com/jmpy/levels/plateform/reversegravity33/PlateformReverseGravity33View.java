package com.jmpy.levels.plateform.reversegravity33;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;

public class PlateformReverseGravity33View extends PlateformView {

	public PlateformReverseGravity33View(MainActivity mainActivity,
			LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		super.init(new PlateformBall(this, new Point(36, 694)), 
				new PlateformReverseGravity33Land(this));
	}

}
