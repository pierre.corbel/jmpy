	package com.jmpy.levels.plateform.reversegravity33;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;
import com.jmpy.levels.plateform.reusable.DeathFall;
import com.jmpy.levels.plateform.reusable.DeathWell;

public class PlateformReverseGravity33Land extends PlateformLand {

	public PlateformReverseGravity33Land(LevelView camera) {
		super(camera);

	    //startPoint.addRect(new RectF(36, 694, 58, 720), Direction.CCW);

	    hitBox.addRect(new RectF(0, 20, 22, 2101), Direction.CCW);

	    dieHitBox.addRect(new RectF(0, 2078, 4457, 2098), Direction.CCW);

	    hitBox.addRect(new RectF(-334, 1068, -314, 1217), Direction.CCW);

	    hitBox.addRect(new RectF(-414, 1381, -395, 1447), Direction.CCW);

	    hitBox.addRect(new RectF(4452, 6, 4474, 2092), Direction.CCW);

	    hitBox.addRect(new RectF(-502, 1294, -207, 1306), Direction.CCW);

	    hitBox.addRect(new RectF(-535, 1012, -115, 1024), Direction.CCW);

	    hitBox.addRect(new RectF(5, 746, 284, 774), Direction.CCW);

	    hitBox.addRect(new RectF(-213, 1129, -183, 1159), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-1133, 1545, -107, 1563), Direction.CCW);

	    hitBox.addRect(new RectF(18, 4, 4471, 24), Direction.CCW);

	    hitBox.addRect(new RectF(-614, 1068, -595, 1200), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-179, -125, -162, 823), Direction.CCW);

	    reverseGravity.addRect(new RectF(1456, 851, 2284, 1446), Direction.CCW);

	    dieHitBox.addRect(new RectF(1794, 500, 2016, 1001), Direction.CCW);

	    hitBox.addRect(new RectF(615, 480, 1008, 605), Direction.CCW);

	    dieHitBox.addRect(new RectF(1275, 590, 1679, 1544), Direction.CCW);

	    hitBox.addRect(new RectF(874, 271, 1293, 396), Direction.CCW);

	    hitBox.addRect(new RectF(2227, 283, 2431, 1720), Direction.CCW);

	    hitBox.addRect(new RectF(-318, 293, -298, 442), Direction.CCW);

	    dieHitBox.addRect(new RectF(618, 272, 875, 397), Direction.CCW);

	    hitBox.addRect(new RectF(616, 569, 1490, 694), Direction.CCW);

	    dieHitBox.addRect(new RectF(2453, 778, 3745, 872), Direction.CCW);

	    hitBox.addRect(new RectF(1846, 88, 1978, 501), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(1925, 83, 1990, 395), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(2211, 283, 2276, 703), Direction.CCW);

	    hitBox.addRect(new RectF(2291, 286, 3044, 411), Direction.CCW);

	    hitBox.addRect(new RectF(2583, 228, 2728, 353), Direction.CCW);

	    hitBox.addRect(new RectF(3227, 498, 3420, 555), Direction.CCW);

	    hitBox.addRect(new RectF(3525, 696, 3718, 753), Direction.CCW);

	    hitBox.addRect(new RectF(3352, 253, 3545, 310), Direction.CCW);

	    hitBox.addRect(new RectF(3650, 451, 3843, 508), Direction.CCW);

	    bounceHitBox.addRect(new RectF(3229, 485, 3419, 503), Direction.CCW);

	    bounceHitBox.addRect(new RectF(3538, 686, 3728, 704), Direction.CCW);

	    hitBox.addRect(new RectF(3437, 1086, 3903, 1211), Direction.CCW);

	    hitBox.addRect(new RectF(3431, 1022, 3576, 1147), Direction.CCW);

	    hitBox.addRect(new RectF(1873, 1957, 3599, 2106), Direction.CCW);

	    hitBox.addRect(new RectF(3065, 1878, 3210, 2003), Direction.CCW);

	    hitBox.addRect(new RectF(2177, 1874, 2322, 1999), Direction.CCW);

	    flagHitbox.addRect(new RectF(1982, 1875, 2004, 1901), Direction.CCW);

	    dieHitBox.addRect(new RectF(1273, 1488, 2270, 1566), Direction.CCW);

	    dieHitBox.addRect(new RectF(2089, 711, 2335, 1557), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(430, 491, 462, 527), Direction.CCW);

	    bounceHitBox.addRect(new RectF(3356, 297, 3546, 315), Direction.CCW);

	    reverseGravity.addRect(new RectF(254, 571, 304, 614), Direction.CCW);

	    dieHitBox.addRect(new RectF(3904, 78, 3986, 1200), Direction.CCW);
/*
		dieHitBox.addCircle(3114,1208, 47, Direction.CCW);

		dieHitBox.addCircle(3272,1530, 47, Direction.CCW);

		dieHitBox.addCircle(-138,883, 47, Direction.CCW);

		dieHitBox.addCircle(1976,447, 47, Direction.CCW);

		dieHitBox.addCircle(2236,644, 47, Direction.CCW);

		dieHitBox.addCircle(2232,310, 47, Direction.CCW);

		dieHitBox.addCircle(1974,214, 47, Direction.CCW);

		dieHitBox.addCircle(3184,1035, 47, Direction.CCW);

		dieHitBox.addCircle(3324,1341, 47, Direction.CCW);

		dieHitBox.addCircle(3322,886, 47, Direction.CCW);

		dieHitBox.addCircle(3414,1188, 47, Direction.CCW);

	    dieHitBox.addCircle(1560,517, 47, Direction.CCW);

	    dieHitBox.addCircle(3075,1382, 47, Direction.CCW);

	    dieHitBox.addCircle(3058,1573, 47, Direction.CCW);
*/
	    
	    getAnimatedLands().add(new DeathWell(2329, 1928, camera, 708));


	    updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
