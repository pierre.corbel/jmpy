package com.jmpy.levels.plateform.death4;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.Ball;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;

public class PlateformDeath4View extends PlateformView {

	public PlateformDeath4View(MainActivity mainActivity,
			LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}

	@Override
	protected void init() {
		super.init(new PlateformBall(this, new Point(31, 409)), 
				new PlateformDeath4Land(this));
	}

}
