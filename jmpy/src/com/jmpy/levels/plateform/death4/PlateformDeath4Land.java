package com.jmpy.levels.plateform.death4;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Path.Direction;
import android.view.animation.Animation;

import com.example.jmpy.R;
import com.jmpy.engine.component.AnimatedLand;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.component.animation.LinearGoAndBackAnimation;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathWall;
import com.jmpy.levels.plateform.reusable.DeathWell;

public class PlateformDeath4Land extends PlateformLand {

	private Bitmap floorBitmap;

	public PlateformDeath4Land(LevelView camera) {
		super(camera);
		
	    hitBox.addRect(new RectF(1, -1, 23, 512), Direction.CCW);

	    hitBox.addRect(new RectF(3111, 62, 3125, 394), Direction.CCW);

	    hitBox.addRect(new RectF(2, 510, 1392, 530), Direction.CCW);

	    flagHitbox.addRect(new RectF(33, 212, 55, 238), Direction.CCW);

	    hitBox.addRect(new RectF(-451, 391, -431, 540), Direction.CCW);

	    hitBox.addRect(new RectF(-954, 519, -935, 585), Direction.CCW);

	    hitBox.addRect(new RectF(7, 440, 196, 514), Direction.CCW);

	    hitBox.addRect(new RectF(1373, 3, 1395, 528), Direction.CCW);

	    hitBox.addRect(new RectF(-1042, 432, -747, 444), Direction.CCW);

	    hitBox.addRect(new RectF(-1223, 621, -803, 633), Direction.CCW);

	    hitBox.addRect(new RectF(1148, 455, 1378, 517), Direction.CCW);

	    hitBox.addRect(new RectF(198, 283, 1203, 313), Direction.CCW);

	    getAnimatedLands().add(new DeathWell(238, 490, camera, 873));

	    wallJumpHitBox.addRect(new RectF(1359, 211, 1378, 387), Direction.CCW);

	    hitBox.addRect(new RectF(23, 239, 212, 313), Direction.CCW);
	    
	    getAnimatedLands().add(new DeathWell(1188, 263, camera, -980));
	    
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		//canvas.drawColor(Color.BLACK);
		//canvas.drawBitmap(floorBitmap, 3, 460, null);
		super.draw(true, canvas);
	}
}
