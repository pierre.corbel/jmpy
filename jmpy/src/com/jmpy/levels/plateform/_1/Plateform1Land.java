package com.jmpy.levels.plateform._1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.ALand;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Plateform1Land extends PlateformLand {

    public Plateform1Land(LevelView camera) {
        super(camera);


        hitBox.addRect(new RectF(0, 1, 22, 514), Direction.CCW);

        hitBox.addRect(new RectF(2761, 63, 2775, 395), Direction.CCW);

        hitBox.addRect(new RectF(0, 512, 959, 594), Direction.CCW);

        hitBox.addRect(new RectF(7, 2, 967, 36), Direction.CCW);

        //startPoint.addRect(new RectF(40, 480, 62, 506), Direction.CCW);

        hitBox.addRect(new RectF(-232, 562, -212, 711), Direction.CCW);

        hitBox.addRect(new RectF(-414, 519, -395, 585), Direction.CCW);

        hitBox.addRect(new RectF(368, 415, 780, 439), Direction.CCW);

        flagHitbox.addRect(new RectF(920, 200, 942, 226), Direction.CCW);

        hitBox.addRect(new RectF(949, 4, 971, 591), Direction.CCW);

        hitBox.addRect(new RectF(-502, 432, -207, 444), Direction.CCW);

        hitBox.addRect(new RectF(-643, 799, -223, 811), Direction.CCW);

        hitBox.addRect(new RectF(18, 325, 235, 349), Direction.CCW);

        hitBox.addRect(new RectF(484, 236, 956, 260), Direction.CCW);

        setFloorBitmap(BitmapFactory.decodeResource(levelView.getResources(), R.drawable.level5));
        updateZeroG();
        top =48;
    }

    @Override
    public void draw(Canvas canvas, boolean showHitBox) {
        super.draw(false, canvas);
    }
}