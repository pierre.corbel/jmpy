package com.jmpy.levels.plateform;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;

import com.example.jmpy.R;
import com.jmpy.MainActivity;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.layout.Controls;
import com.jmpy.layout.OnControlTouch.ACCELERATION_TYPE;

public class PlateformControl extends Controls {

	private static final int JUMP_CENTER_MARGIN = 10;
	private static final int NB_JUMP_VARIATIONS = 4;
	private static final int JUMP_VARIATIONS_WAIT_FOR_FRAME = 9;
	private Bitmap left;
	private Bitmap right;
	private Rect leftHitBox;
	private Rect rightHitBox;
	private int currentFrameSinceJumpStart;
	private Vector2D controlCenter;
	


	public Vector2D getControlCenter() {
		return controlCenter;
	}

	public PlateformControl(Context context, LevelView camera) {
		super(context, camera, (float)6.2, 80);
		this.left = BitmapFactory.decodeResource(getResources(), R.drawable.left);
		this.right = BitmapFactory.decodeResource(getResources(), R.drawable.right);
		setOnTouchListener(new OnPlateformControlTouch());
	}
	
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		defineJumpHitBox();
		leftHitBox = new Rect(0, getHeight() - left.getHeight() -3*BUTTON_PADDING, BUTTON_PADDING +left.getWidth()+BUTTON_PADDING, getHeight());
		rightHitBox = new Rect(leftHitBox.right+1, getHeight() - right.getHeight() -3*BUTTON_PADDING,leftHitBox.right + BUTTON_PADDING + right.getWidth() + BUTTON_PADDING, getHeight());
		if(!isShowHint()) {
			canvas.drawBitmap(left, leftHitBox.left +BUTTON_PADDING, leftHitBox.top + BUTTON_PADDING, null);
			canvas.drawBitmap(right, rightHitBox.left +BUTTON_PADDING, rightHitBox.top +BUTTON_PADDING , null);
			evaluateJumpVariation();
			drawJumpCircle(canvas);
		}
	}


	private void defineJumpHitBox () {
		if(controlCenter==null) {
			controlCenter = new Vector2D(levelView.getWidth()-(maxDistance + JUMP_CENTER_MARGIN*this.zoomResolution), levelView.getHeight()-(maxDistance + JUMP_CENTER_MARGIN*this.zoomResolution));
			jumpCenterHitBox = new PointF(
					(float) controlCenter.x,
					(float) controlCenter.y);
			jumpHitBox = new Rect((int) (jumpCenterHitBox.x - (maxDistance + JUMP_CENTER_MARGIN)*this.zoomResolution),
					0,
					(int) (jumpCenterHitBox.x +  (maxDistance + JUMP_CENTER_MARGIN)*this.zoomResolution),
					getHeight());
		}
	}

	protected void drawJumpCircle(Canvas canvas) {
		// draw jump zone
		paint.reset();
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		paint.setAlpha(150);
		canvas.drawCircle(jumpCenterHitBox.x, jumpCenterHitBox.y, maxDistance, paint);
		paint.setColor(Color.BLACK);
		paint.setAlpha(150);
		canvas.drawCircle(jumpCenterHitBox.x, jumpCenterHitBox.y, 10*zoomResolution, paint);

		
		// draw jump vector
		if (previousJumpControlVector != null) {
			paint.setStrokeWidth(zoomResolution);
			canvas.drawLine(
					(float) controlCenter.x,
					(float) controlCenter.y,
					(float) (controlCenter.x + previousJumpControlVector.x
							* maxDistance / maxAccelerate),
					(float) (controlCenter.y + previousJumpControlVector.y
							* maxDistance / maxAccelerate), paint);
		}
	}
	
	private void evaluateJumpVariation() {
		if(previousJumpControlVector!=null && previousJumpControlVector.isFullTop() && currentFrameSinceJumpStart < NB_JUMP_VARIATIONS*JUMP_VARIATIONS_WAIT_FOR_FRAME) {

			if(currentFrameSinceJumpStart%JUMP_VARIATIONS_WAIT_FOR_FRAME==0) {
				int jumpImpulsionIteration = Math.round(currentFrameSinceJumpStart/JUMP_VARIATIONS_WAIT_FOR_FRAME+1);
				double power = previousJumpControlVector.getNorm()/Math.pow(NB_JUMP_VARIATIONS-(NB_JUMP_VARIATIONS-jumpImpulsionIteration), 1.5);
				if (previousJumpControlVector.isSmallBottom()  && levelView.getWallJumpData()!=null)
					"".equals(null);
				if(currentFrameSinceJumpStart==0 && levelView.getWallJumpData()!=null) {
					currentFrameSinceJumpStart += JUMP_VARIATIONS_WAIT_FOR_FRAME;
					jumpImpulsionIteration += 1;
					power += previousJumpControlVector.getNorm()/Math.pow(NB_JUMP_VARIATIONS-(NB_JUMP_VARIATIONS-jumpImpulsionIteration), 1.5);					
				}
				
				
				jumpControlVector = Vector2D.createFromTethaAngleAndNorm(previousJumpControlVector.getThetaAngle(), power);
			} else{
				jumpControlVector = null;
			}
		} else if (previousJumpControlVector!=null && previousJumpControlVector.isBottom()) {
			jumpControlVector = Vector2D.createFromTethaAngleAndNorm(previousJumpControlVector.getThetaAngle(), previousJumpControlVector.getNorm());
			previousJumpControlVector = null;
		} else {
			jumpControlVector = null;
		}
		currentFrameSinceJumpStart++;
	}
	
	public Rect getLeftHitBox() {
		return leftHitBox;
	}

	public Rect getRightHitBox() {
		return rightHitBox;
	}
	

	@Override
	public void calculateFuturPushAcceleration(Vector2D pushDistance,
			ACCELERATION_TYPE move) {
		
		Vector2D controlVector;
		if (pushDistance == null) {
			controlVector = null;
		} else {
			double distance = pushDistance.getNorm()<maxDistance?pushDistance.getNorm():maxDistance;
			distance = maxDistance;
			double power = distance / maxDistance * maxAccelerate;
			controlVector = Vector2D.createFromTethaAngleAndNorm(
					pushDistance.getThetaAngle(), power);
		}
		if (move == ACCELERATION_TYPE.JUMP) {
			boolean justTouchJumpButon = controlVector!=null && previousJumpControlVector==null;
			if (justTouchJumpButon && (levelView.getNbFrameSinceInTheSky()<5 || controlVector.isBottom() || levelView.getWallJumpData()!=null)) {
				currentFrameSinceJumpStart = 0;				
				if(!controlVector.isBottom() || (controlVector.isBottom() && levelView.getBall().getSpeed().y<0)) {
					this.levelView.stopTheBall();
				}
			}
			previousJumpControlVector = controlVector;
		} else {
			moveControlVector = controlVector;
		}
	}
	
}
