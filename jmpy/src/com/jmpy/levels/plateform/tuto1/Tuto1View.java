package com.jmpy.levels.plateform.tuto1;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformView;
import com.jmpy.levels.zerog._1.ZeroG1Land;

public class Tuto1View extends PlateformView {

	public Tuto1View(MainActivity mainActivity, LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void init() {
		super.init(new PlateformBall(this, new Point(753,234)), new Tuto1Land(this));
	}

}
