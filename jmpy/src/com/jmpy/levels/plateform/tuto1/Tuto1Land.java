package com.jmpy.levels.plateform.tuto1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Tuto1Land extends PlateformLand {

	public Tuto1Land(LevelView camera) {
		super(camera);

	    hitBox.addRect(new RectF(0, 1, 22, 355), Direction.CCW);

	    hitBox.addRect(new RectF(1538, 45, 1552, 377), Direction.CCW);

	    hitBox.addRect(new RectF(0, 354, 1560, 374), Direction.CCW);

	    hitBox.addRect(new RectF(193, 262, 1342, 282), Direction.CCW);

	    hitBox.addRect(new RectF(694, 46, 717, 263), Direction.CCW);

	    hitBox.addRect(new RectF(-1899, 704, -1879, 853), Direction.CCW);

	    hitBox.addRect(new RectF(2089, 848, 2100, 924), Direction.CCW);

	    hitBox.addRect(new RectF(790, -2298, 803, -2000), Direction.CCW);

	    flagHitbox.addRect(new RectF(645, 235, 667, 261), Direction.CCW);
	    
		setFloorBitmap(BitmapFactory.decodeResource(camera.getResources(), R.drawable.tuto1));
		left = 4;
		top = -26;
	    updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(false, canvas);
	}

}
