package com.jmpy.levels.plateform.tuto4;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Tuto4Land extends PlateformLand {

	public Tuto4Land(LevelView camera) {
		super(camera);

	    hitBox.addRect(new RectF(0, 1, 22, 514), Direction.CCW);

	    hitBox.addRect(new RectF(2761, 63, 2775, 395), Direction.CCW);

	    hitBox.addRect(new RectF(0, 512, 1768, 532), Direction.CCW);

	    hitBox.addRect(new RectF(417, 440, 839, 514), Direction.CCW);

	    hitBox.addRect(new RectF(-133, 474, -113, 623), Direction.CCW);

	    hitBox.addRect(new RectF(-414, 519, -395, 585), Direction.CCW);

	    hitBox.addRect(new RectF(1065, 441, 1770, 515), Direction.CCW);

	    hitBox.addRect(new RectF(1353, 326, 1768, 446), Direction.CCW);

	    hitBox.addRect(new RectF(1830, 1, 1852, 614), Direction.CCW);

	    hitBox.addRect(new RectF(1203, 594, 1851, 614), Direction.CCW);

	    this.bonusFlagHitbox.addRect(new RectF(1236, 568, 1258, 594), Direction.CCW);

	    flagHitbox.addRect(new RectF(1500, 297, 1522, 323), Direction.CCW);

	    hitBox.addRect(new RectF(1195, 516, 1217, 679), Direction.CCW);

	    hitBox.addRect(new RectF(-502, 432, -207, 444), Direction.CCW);

	    hitBox.addRect(new RectF(-683, 621, -263, 633), Direction.CCW);

	    dieHitBox.addCircle(954,408, 47, Direction.CCW);

	    dieHitBox.addCircle(1666,263, 47, Direction.CCW);


		setFloorBitmap(BitmapFactory.decodeResource(levelView.getResources(), R.drawable.tuto4));
		//floorBitmap = Bitmap.createBitmap(floorBitmap, 0, 0,500, 500);
		updateZeroG();
		left = 0;
		top = 17;
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		
		super.draw(false, canvas);
	}

}
