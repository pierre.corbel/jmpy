package com.jmpy.levels.plateform.tuto2;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;

public class Tuto2View extends PlateformView {

	public Tuto2View(MainActivity mainActivity, LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}

	@Override
	protected void init() {
		super.init(new PlateformBall(this, new Point(44, 513)), 
				new Tuto2Land(this));
	}

}
