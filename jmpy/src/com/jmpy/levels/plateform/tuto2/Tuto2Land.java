package com.jmpy.levels.plateform.tuto2;

import android.graphics.*;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Tuto2Land extends PlateformLand {

    public Tuto2Land(LevelView camera) {
        super(camera);

        hitBox.addRect(new RectF(1, 5, 23, 570), Direction.CCW);

        hitBox.addRect(new RectF(6, 0, 766, 26), Direction.CCW);

        hitBox.addRect(new RectF(448, 418, 458, 539), Direction.CCW);

        hitBox.addRect(new RectF(405, 1294, 416, 1370), Direction.CCW);

        hitBox.addRect(new RectF(1235, -614, 1248, -316), Direction.CCW);

        hitBox.addRect(new RectF(738, 1, 767, 600), Direction.CCW);

        hitBox.addRect(new RectF(12, 532, 764, 582), Direction.CCW);

        bounceHitBox.addRect(new RectF(-307, 1173, -283, 1191), Direction.CCW);

        wallJumpHitBox.addRect(new RectF(-212, 1194, -185, 1214), Direction.CCW);

        dieHitBox.addRect(new RectF(-260, 1182, -238, 1200), Direction.CCW);

        flagHitbox.addCircle(-227, 1263, 15, Direction.CCW);

        hitBox.addRect(new RectF(209, 459, 220, 535), Direction.CCW);

        hitBox.addRect(new RectF(243, 1370, 253, 1519), Direction.CCW);

        wallJumpHitBox.addRect(new RectF(-157, 1211, -130, 1231), Direction.CCW);

        flagHitbox.addCircle(715, 514, 15, Direction.CCW);


        setFloorBitmap(BitmapFactory.decodeResource(camera.getResources(), R.drawable.tuto2));
        left = 1;
        top = 262;


        updateZeroG();
    }

    @Override
    public void draw(Canvas canvas, boolean showHitBox) {
        super.draw(false, canvas);
    }

}
