package com.jmpy.levels.plateform.death2;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;
import com.jmpy.levels.plateform.reusable.DeathFall;
import com.jmpy.levels.plateform.reusable.DeathWell;

public class PlateformDeath2Land extends PlateformLand {

	public PlateformDeath2Land(LevelView camera) {
		super(camera);
		  

		 
		  
	    hitBox.addRect(new RectF(3, 3, 25, 618), Direction.CCW);

	    hitBox.addRect(new RectF(16, 0, 2863, 26), Direction.CCW);

	    hitBox.addRect(new RectF(405, 1294, 416, 1370), Direction.CCW);

	    hitBox.addRect(new RectF(1074, -1231, 1087, -933), Direction.CCW);

	    hitBox.addRect(new RectF(2843, 6, 2872, 666), Direction.CCW);

	    hitBox.addRect(new RectF(3, 618, 2843, 668), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-307, 1173, -283, 1191), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-212, 1194, -185, 1214), Direction.CCW);

	    dieHitBox.addRect(new RectF(-260, 1182, -238, 1200), Direction.CCW);

	    flagHitbox.addCircle(-227,1263, 15, Direction.CCW);

	    hitBox.addRect(new RectF(204, 890, 214, 1039), Direction.CCW);

	    //startPoint.addCircle(-289,1247, 15, Direction.CCW);

	    hitBox.addRect(new RectF(19, 197, 827, 351), Direction.CCW);

	    hitBox.addRect(new RectF(819, 374, 2747, 435), Direction.CCW);

	    hitBox.addRect(new RectF(811, 326, 1615, 387), Direction.CCW);

	    hitBox.addRect(new RectF(2652, 326, 2747, 387), Direction.CCW);

	    //startPoint.addCircle(51,184, 15, Direction.CCW);

	    hitBox.addRect(new RectF(513, 497, 724, 629), Direction.CCW);

	    flagHitbox.addCircle(42,605, 15, Direction.CCW);

	    hitBox.addRect(new RectF(1421, 548, 1618, 660), Direction.CCW);

	    hitBox.addRect(new RectF(2139, 547, 2299, 650), Direction.CCW);
	  
	    getAnimatedLands().add(new DeathWell(1648, 604, camera, 462, 0));
		    
	    getAnimatedLands().add(new DeathFall(new RectF(287, 19, 419, 201), levelView, 40));
	    getAnimatedLands().add(new DeathFall(new RectF(421, 19, 553, 201), levelView, 20));
	    getAnimatedLands().add(new DeathFall(new RectF(554, 18, 686, 200), levelView, 0));
	    getAnimatedLands().add(new DeathFall(new RectF(682, 25, 814, 207), levelView, -20));
	    getAnimatedLands().add(new DeathFall(new RectF(1095, 12, 2749, 327), levelView, -40));
	  

	    
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
