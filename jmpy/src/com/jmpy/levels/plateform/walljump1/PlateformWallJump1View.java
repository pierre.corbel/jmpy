package com.jmpy.levels.plateform.walljump1;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.Ball;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;

public class PlateformWallJump1View extends PlateformView {

	public PlateformWallJump1View(MainActivity mainActivity,
			LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}

	@Override
	protected void init() {
		super.init(new PlateformBall(this, new Point(181, 1027)), 
				new PlateformWallJump1Land(this));
	}

}
