package com.jmpy.levels.plateform.walljump1;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class PlateformWallJump1Land extends PlateformLand {

	

	public PlateformWallJump1Land(LevelView camera) {
		super(camera);
	    hitBox.addRect(new RectF(3, 469, 25, 1113), Direction.CCW);

	    hitBox.addRect(new RectF(0, 464, 1484, 490), Direction.CCW);

	    hitBox.addRect(new RectF(1453, 461, 1467, 1055), Direction.CCW);

	    hitBox.addRect(new RectF(7, 1129, 1604, 1149), Direction.CCW);

	    hitBox.addRect(new RectF(14, 1039, 513, 1059), Direction.CCW);

	    flagHitbox.addRect(new RectF(1264, 761, 1286, 787), Direction.CCW);

	    hitBox.addRect(new RectF(461, 795, 680, 1001), Direction.CCW);

	    hitBox.addRect(new RectF(277, 1175, 287, 1324), Direction.CCW);

	    hitBox.addRect(new RectF(456, 1192, 467, 1268), Direction.CCW);

	    hitBox.addRect(new RectF(1166, -906, 1179, -608), Direction.CCW);

	    hitBox.addRect(new RectF(249, 788, 920, 864), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(11, 536, 33, 997), Direction.CCW);

	    dieHitBox.addRect(new RectF(516, 1044, 1452, 1128), Direction.CCW);

	    hitBox.addRect(new RectF(1168, 788, 1290, 840), Direction.CCW);

	    dieHitBox.addCircle(775,799, 44, Direction.CCW);

	    hitBox.addRect(new RectF(1277, -1029, 1288, -607), Direction.CCW);
	  
									
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(true, canvas);
	}

}
