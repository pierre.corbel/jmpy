package com.jmpy.levels.plateform.death1;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;

public class PlateformDeath1Land extends PlateformLand {

	public PlateformDeath1Land(LevelView camera) {
		super(camera);


		  
	    hitBox.addRect(new RectF(3, 3, 25, 618), Direction.CCW);

	    hitBox.addRect(new RectF(0, 0, 2312, 26), Direction.CCW);

	    hitBox.addRect(new RectF(405, 1294, 416, 1370), Direction.CCW);


	    hitBox.addRect(new RectF(2283, 6, 2312, 666), Direction.CCW);

	    hitBox.addRect(new RectF(4, 618, 2287, 668), Direction.CCW);





	    hitBox.addRect(new RectF(204, 890, 214, 1039), Direction.CCW);

	    //startPoint.addCircle(1400,593, 15, Direction.CCW);

	    //startPoint.addCircle(-289,1247, 15, Direction.CCW);

	    dieHitBox.addCircle(-140,1311, 44, Direction.CCW);

	    hitBox.addRect(new RectF(237, 428, 1477, 477), Direction.CCW);

	    hitBox.addRect(new RectF(1053, 400, 1154, 449), Direction.CCW);

	    hitBox.addRect(new RectF(787, 398, 888, 447), Direction.CCW);

	    hitBox.addRect(new RectF(475, 398, 591, 447), Direction.CCW);

	    flagHitbox.addCircle(2229,293, 15, Direction.CCW);

	    hitBox.addRect(new RectF(1301, 572, 1343, 621), Direction.CCW);

	    hitBox.addRect(new RectF(23, 572, 297, 621), Direction.CCW);

	    hitBox.addRect(new RectF(1455, 434, 1497, 638), Direction.CCW);

	    hitBox.addRect(new RectF(1721, 527, 2300, 641), Direction.CCW);

	    hitBox.addRect(new RectF(1291, 360, 1556, 445), Direction.CCW);

	    hitBox.addRect(new RectF(1899, 371, 2283, 533), Direction.CCW);

	    hitBox.addRect(new RectF(2086, 314, 2297, 428), Direction.CCW);



		getAnimatedLands().add(new DeathBlink(new RectF(2098, 11, 2106, 317), camera,170, -90, (float)0.25));
		getAnimatedLands().add(new DeathBlink(new RectF(1796, 20, 1804, 538), camera,170, -15, (float)0.25));
		getAnimatedLands().add(new DeathBlink(new RectF(10, 367, 1464, 374), camera,170, 125, (float)0.25));
		getAnimatedLands().add(new DeathBlink(new RectF(277, 597, 1309, 604), camera, 250, 20, (float)0.25));
	    
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
