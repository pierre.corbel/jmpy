	package com.jmpy.levels.plateform.reversegravity3;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;
import com.jmpy.levels.plateform.reusable.DeathFall;

public class PlateformReverseGravity3Land extends PlateformLand {

	public PlateformReverseGravity3Land(LevelView camera) {
		super(camera);

	    hitBox.addRect(new RectF(3, 3, 25, 1056), Direction.CCW);

	    hitBox.addRect(new RectF(-0, 0, 2596, 26), Direction.CCW);

	    hitBox.addRect(new RectF(-591, 734, -580, 810), Direction.CCW);

	    bounceHitBox.addRect(new RectF(-689, 998, -665, 1016), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(-594, 1019, -567, 1039), Direction.CCW);

	    flagHitbox.addCircle(-609,1088, 15, Direction.CCW);

	    hitBox.addRect(new RectF(-924, 962, -914, 1111), Direction.CCW);

	    //startPoint.addCircle(-671,1072, 15, Direction.CCW);

	    dieHitBox.addCircle(-522,1136, 44, Direction.CCW);

	    flagHitbox.addCircle(461,561, 15, Direction.CCW);

	    //startPoint.addCircle(73,947, 15, Direction.CCW);

	    dieHitBox.addRect(new RectF(-647, 1004, -625, 1022), Direction.CCW);

	    hitBox.addRect(new RectF(-789, 427, -484, 447), Direction.CCW);

	    hitBox.addRect(new RectF(14, 969, 164, 1061), Direction.CCW);

	    hitBox.addRect(new RectF(10, 582, 1890, 836), Direction.CCW);

	    dieHitBox.addCircle(753,839, 44, Direction.CCW);

	    dieHitBox.addCircle(1310,839, 44, Direction.CCW);

	    dieHitBox.addRect(new RectF(163, 972, 596, 1060), Direction.CCW);

	    hitBox.addRect(new RectF(596, 972, 1456, 1064), Direction.CCW);

	    bounceHitBox.addRect(new RectF(596, 965, 1453, 983), Direction.CCW);

	    reverseGravity.addRect(new RectF(16, 822, 772, 860), Direction.CCW);

	    dieHitBox.addRect(new RectF(1441, 977, 1791, 1065), Direction.CCW);

	    hitBox.addRect(new RectF(1783, 970, 2199, 1062), Direction.CCW);

	    dieHitBox.addRect(new RectF(2192, 975, 2549, 1054), Direction.CCW);

	    hitBox.addRect(new RectF(2106, 569, 2389, 838), Direction.CCW);

	    hitBox.addRect(new RectF(2554, 962, 3105, 1054), Direction.CCW);

	    hitBox.addRect(new RectF(2658, 0, 2705, 1054), Direction.CCW);

	    reverseGravity.addRect(new RectF(2387, 530, 2668, 844), Direction.CCW);

	    hitBox.addRect(new RectF(637, 550, 825, 601), Direction.CCW);

	    bounceHitBox.addRect(new RectF(2106, 562, 2385, 580), Direction.CCW);

	    reverseGravity.addRect(new RectF(1299, 828, 2670, 878), Direction.CCW);

	    zeroGHitBox.addRect(new RectF(5, 811, 770, 839), Direction.CCW);

	    zeroGHitBox.addRect(new RectF(2108, 819, 2383, 847), Direction.CCW);

	    zeroGHitBox.addRect(new RectF(1297, 815, 1881, 843), Direction.CCW);

	    bonusFlagHitbox.addCircle(1998,727, 15, Direction.CCW);

	    hitBox.addRect(new RectF(831, 286, 1882, 597), Direction.CCW);

	    bounceHitBox.addRect(new RectF(1084, 264, 1866, 282), Direction.CCW);

	    dieHitBox.addCircle(1541,275, 44, Direction.CCW);

	    dieHitBox.addCircle(1100,272, 44, Direction.CCW);

	    dieHitBox.addCircle(800,297, 44, Direction.CCW);

	    dieHitBox.addCircle(897,150, 44, Direction.CCW);

	    dieHitBox.addCircle(591,337, 44, Direction.CCW);

	    dieHitBox.addCircle(455,209, 44, Direction.CCW);

	    dieHitBox.addCircle(693,286, 44, Direction.CCW);

	    dieHitBox.addCircle(656,99, 44, Direction.CCW);

	    dieHitBox.addCircle(362,351, 44, Direction.CCW);
	    
	    
	    getAnimatedLands().add(new DeathFall(new RectF(1897, 577, 2098, 967), camera, 100));

	    updateZeroG();

	    zeroGHitBox.addRect(new RectF(5, 811, 770, 839), Direction.CCW);

	    zeroGHitBox.addRect(new RectF(2108, 819, 2383, 847), Direction.CCW);

	    zeroGHitBox.addRect(new RectF(1297, 815, 1881, 843), Direction.CCW);
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
