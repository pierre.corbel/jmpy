package com.jmpy.levels.plateform._2;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.engine.component.Ball;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.physic.Vector2D;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.landscape.LandscapeBall;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;

public class Plateform2View extends PlateformView {

	public Plateform2View(MainActivity mainActivity, LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}

	@Override
	protected void init() {
		super.init(new PlateformBall(this, new Point(71, 157)), 
			new Plateform2Land(this));
	}
}
