package com.jmpy.levels.plateform._2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.Path.Direction;

import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Plateform2Land extends PlateformLand {

	public Plateform2Land(LevelView camera) {
		super(camera);
		  
		  
	    hitBox.addRect(new RectF(0, 1, 22, 830), Direction.CCW);

	    hitBox.addRect(new RectF(2761, 63, 2775, 395), Direction.CCW);

	    hitBox.addRect(new RectF(0, 792, 959, 874), Direction.CCW);

	    hitBox.addRect(new RectF(7, 2, 967, 36), Direction.CCW);

	    //startPoint.addRect(new RectF(71, 157, 93, 183), Direction.CCW);

	    hitBox.addRect(new RectF(-414, 519, -395, 585), Direction.CCW);

	    hitBox.addRect(new RectF(1250, 206, 1662, 230), Direction.CCW);

	    flagHitbox.addRect(new RectF(917, 645, 939, 671), Direction.CCW);

	    hitBox.addRect(new RectF(949, 4, 971, 877), Direction.CCW);

	    hitBox.addRect(new RectF(-502, 432, -207, 444), Direction.CCW);

	    hitBox.addRect(new RectF(-643, 799, -223, 811), Direction.CCW);

	    hitBox.addRect(new RectF(202, 387, 654, 411), Direction.CCW);

	    hitBox.addRect(new RectF(25, 208, 497, 232), Direction.CCW);

	    hitBox.addRect(new RectF(642, 22, 678, 403), Direction.CCW);

	    hitBox.addRect(new RectF(202, 302, 266, 405), Direction.CCW);

	    hitBox.addRect(new RectF(548, 568, 655, 799), Direction.CCW);

	    hitBox.addRect(new RectF(285, 669, 477, 690), Direction.CCW);

	    hitBox.addRect(new RectF(908, 678, 950, 793), Direction.CCW);

	    hitBox.addRect(new RectF(-191, 523, -161, 672), Direction.CCW);

		setFloorBitmap(BitmapFactory.decodeResource(levelView.getResources(), R.drawable.level6));
	    updateZeroG();
		top = -3;
		left = - 23;
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(false, canvas);
	}

}
