	package com.jmpy.levels.plateform.reversegravity1;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;

public class PlateformReverseGravity1Land extends PlateformLand {

	public PlateformReverseGravity1Land(LevelView camera) {
		super(camera);
		  
		  
	    reverseGravity.addRect(new RectF(1580, 1075, 1768, 1250), Direction.CCW);

	    hitBox.addRect(new RectF(0, 74, 22, 1759), Direction.CCW);

	    hitBox.addRect(new RectF(0, 1568, 2793, 1682), Direction.CCW);


	    hitBox.addRect(new RectF(2780, 78, 2802, 1710), Direction.CCW);


	    dieHitBox.addRect(new RectF(421, 999, 1060, 1115), Direction.CCW);

	    hitBox.addRect(new RectF(2, 2, 2807, 88), Direction.CCW);

	    flagHitbox.addRect(new RectF(869, 346, 891, 372), Direction.CCW);

	    reverseGravity.addRect(new RectF(1300, 1331, 1488, 1506), Direction.CCW);

	    dieHitBox.addCircle(1584,1242, 47, Direction.CCW);

	    dieHitBox.addCircle(312,1562, 47, Direction.CCW);

	    reverseGravity.addRect(new RectF(1394, 1251, 1582, 1426), Direction.CCW);

	    reverseGravity.addRect(new RectF(1486, 1161, 1674, 1336), Direction.CCW);

	    reverseGravity.addRect(new RectF(1674, 987, 1862, 1162), Direction.CCW);

	    reverseGravity.addRect(new RectF(1768, 907, 1956, 1082), Direction.CCW);

	    reverseGravity.addRect(new RectF(1860, 817, 2048, 992), Direction.CCW);

	    reverseGravity.addRect(new RectF(220, 1105, 437, 1428), Direction.CCW);
	  
	    hitBox.addRect(new RectF(422, 1021, 549, 1597), Direction.CCW);

	    dieHitBox.addCircle(584,822, 47, Direction.CCW);

	    reverseGravity.addRect(new RectF(2488, 78, 2716, 979), Direction.CCW);

	    reverseGravity.addRect(new RectF(1351, 83, 2491, 350), Direction.CCW);

	    hitBox.addRect(new RectF(814, 379, 1219, 407), Direction.CCW);
	    
	    dieHitBox.addCircle(1840,110, 47, Direction.CCW);
	    
	    dieHitBox.addCircle(2662,110, 47, Direction.CCW);
	    
	    zeroGHitBox.addRect(new RectF(1356, 68, 2690, 96), Direction.CCW);
	    
	    updateZeroG();

	    zeroGHitBox.addRect(new RectF(1356, 68, 2690, 96), Direction.CCW);
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
