package com.jmpy.levels.plateform.death3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Path.Direction;
import android.view.animation.Animation;

import com.example.jmpy.R;
import com.jmpy.engine.component.AnimatedLand;
import com.jmpy.engine.component.LevelView;
import com.jmpy.engine.component.animation.LinearGoAndBackAnimation;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathWall;

public class PlateformDeath3Land extends PlateformLand {

	private Bitmap floorBitmap;

	public PlateformDeath3Land(LevelView camera) {
		super(camera);


		hitBox.addRect(new RectF(1, -1, 23, 512), Direction.CCW);

		hitBox.addRect(new RectF(3111, 62, 3125, 394), Direction.CCW);

		hitBox.addRect(new RectF(2, 510, 2002, 530), Direction.CCW);

		flagHitbox.addRect(new RectF(87, 482, 109, 508), Direction.CCW);

		//startPoint.addRect(new RectF(438, 479, 460, 505), Direction.CCW);

		hitBox.addRect(new RectF(-673, 474, -653, 623), Direction.CCW);

		hitBox.addRect(new RectF(-954, 519, -935, 585), Direction.CCW);

		hitBox.addRect(new RectF(617, 440, 1087, 514), Direction.CCW);

		hitBox.addRect(new RectF(1983, 3, 2005, 528), Direction.CCW);

		hitBox.addRect(new RectF(-1042, 432, -747, 444), Direction.CCW);

		hitBox.addRect(new RectF(-1223, 621, -803, 633), Direction.CCW);

		dieHitBox.addCircle(955,433, 47, Direction.CCW);

		hitBox.addRect(new RectF(1562, 445, 1796, 518), Direction.CCW);

		hitBox.addRect(new RectF(1075, 246, 1866, 258), Direction.CCW);

		hitBox.addRect(new RectF(1370, 173, 1501, 247), Direction.CCW);

		//dieHitBox.addRect(new RectF(255, 269, 357, 521), Direction.CCW);

		wallJumpHitBox.addRect(new RectF(1866, 309, 1899, 350), Direction.CCW);

		bounceHitBox.addRect(new RectF(1970, 120, 1996, 372), Direction.CCW);

	    
	    getAnimatedLands().add(new DeathWall(255, 269, levelView, 1600, 0, 50, 300));
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		//canvas.drawColor(Color.BLACK);
		//canvas.drawBitmap(floorBitmap, 3, 460, null);
		super.draw(true, canvas);
	}
}
