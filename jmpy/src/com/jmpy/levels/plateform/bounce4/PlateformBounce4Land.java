	package com.jmpy.levels.plateform.bounce4;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class PlateformBounce4Land extends PlateformLand {

	public PlateformBounce4Land(LevelView camera) {
		super(camera);
	    hitBox.addRect(new RectF(0, 20, 22, 1375), Direction.CCW);

	    hitBox.addRect(new RectF(2761, 63, 2775, 395), Direction.CCW);

	    hitBox.addRect(new RectF(0, 1374, 2081, 1394), Direction.CCW);

	    hitBox.addRect(new RectF(417, 1302, 685, 1376), Direction.CCW);

	    flagHitbox.addRect(new RectF(1695, 159, 1717, 185), Direction.CCW);

	    hitBox.addRect(new RectF(458, 1460, 478, 1609), Direction.CCW);

	    hitBox.addRect(new RectF(-414, 1381, -395, 1447), Direction.CCW);

	    hitBox.addRect(new RectF(2044, 6, 2066, 1476), Direction.CCW);

	    hitBox.addRect(new RectF(-502, 1294, -207, 1306), Direction.CCW);

	    hitBox.addRect(new RectF(-535, 1012, -115, 1024), Direction.CCW);

	    dieHitBox.addCircle(672,1276, 47, Direction.CCW);

	    hitBox.addRect(new RectF(89, 1181, 368, 1209), Direction.CCW);

	    hitBox.addRect(new RectF(752, 952, 843, 1176), Direction.CCW);

	    dieHitBox.addCircle(2037,799, 47, Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(746, 951, 763, 1175), Direction.CCW);

	    hitBox.addRect(new RectF(-213, 1139, -193, 1159), Direction.CCW);

	    dieHitBox.addCircle(1017,1303, 47, Direction.CCW);

	    dieHitBox.addCircle(809,1269, 47, Direction.CCW);

	    bounceHitBox.addRect(new RectF(1063, 1361, 2089, 1379), Direction.CCW);

	    dieHitBox.addCircle(1242,1317, 47, Direction.CCW);

	    dieHitBox.addCircle(1454,1313, 47, Direction.CCW);

	    hitBox.addRect(new RectF(1670, 185, 1842, 914), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(1836, 266, 1853, 913), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(2035, 38, 2052, 1036), Direction.CCW);

	    hitBox.addRect(new RectF(18, 4, 2051, 24), Direction.CCW);

	    hitBox.addRect(new RectF(1000, 1329, 1028, 1403), Direction.CCW);

	    dieHitBox.addCircle(1846,592, 47, Direction.CCW);

	    dieHitBox.addCircle(2047,403, 47, Direction.CCW);

	    bonusFlagHitbox.addRect(new RectF(26, 878, 48, 904), Direction.CCW);

	    dieHitBox.addCircle(668,1178, 47, Direction.CCW);

	    dieHitBox.addCircle(1708,1312, 47, Direction.CCW);

	    hitBox.addRect(new RectF(-614, 1068, -595, 1200), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(15, -95, 32, 853), Direction.CCW);

	    updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
