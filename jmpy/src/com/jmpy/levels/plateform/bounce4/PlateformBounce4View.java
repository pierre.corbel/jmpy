package com.jmpy.levels.plateform.bounce4;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;
import com.jmpy.levels.plateform.bounce1.PlateformBounce1Land;

public class PlateformBounce4View extends PlateformView {

	public PlateformBounce4View(MainActivity mainActivity,
			LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		super.init(new PlateformBall(this, new Point(40, 1341)), 
				new PlateformBounce4Land(this));
	}


}
