package com.jmpy.levels.plateform.tuto9;

import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;
import com.example.jmpy.R;
import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;

public class Tuto9Land extends PlateformLand {

	public Tuto9Land(LevelView camera) {
		super(camera);
		hitBox.addRect(new RectF(3, 469, 25, 1709), Direction.CCW);
		hitBox.addRect(new RectF(0, 464, 795, 490), Direction.CCW);
		hitBox.addRect(new RectF(7, 1689, 789, 1709), Direction.CCW);
		flagHitbox.addRect(new RectF(1264, 761, 1286, 787), Direction.CCW);
		hitBox.addRect(new RectF(987, 1451, 997, 1600), Direction.CCW);
		hitBox.addRect(new RectF(914, 1186, 925, 1262), Direction.CCW);
		hitBox.addRect(new RectF(1272, -1210, 1285, -912), Direction.CCW);
		wallJumpHitBox.addRect(new RectF(11, 1042, 33, 1602), Direction.CCW);
		hitBox.addRect(new RectF(1168, 788, 1290, 840), Direction.CCW);
		hitBox.addRect(new RectF(1336, -1252, 1347, -830), Direction.CCW);
		wallJumpHitBox.addRect(new RectF(154, 778, 176, 1610), Direction.CCW);
		hitBox.addRect(new RectF(775, 476, 797, 1716), Direction.CCW);
		hitBox.addRect(new RectF(161, 661, 674, 748), Direction.CCW);
		wallJumpHitBox.addRect(new RectF(14, 610, 36, 852), Direction.CCW);
		flagHitbox.addRect(new RectF(596, 631, 617, 653), Direction.CCW);
		hitBox.addRect(new RectF(161, 675, 253, 1606), Direction.CCW);

		setFloorBitmap(BitmapFactory.decodeResource(levelView.getResources(), R.drawable.tuto9));

		top = 464;
		left = -3;
		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {
		super.draw(false, canvas);
	}

}
