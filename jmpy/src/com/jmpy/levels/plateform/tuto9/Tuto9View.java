package com.jmpy.levels.plateform.tuto9;

import android.graphics.Point;
import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformView;

public class Tuto9View extends PlateformView {

	public Tuto9View(MainActivity mainActivity, LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
	}

	@Override
	protected void init() {
		super.init(new PlateformBall(this, new Point(679, 1658)),
				new Tuto9Land(this));
	}

}
