package com.jmpy.levels.plateform.doublejump0;

import android.graphics.Point;

import com.jmpy.MainActivity;
import com.jmpy.levels.data.LevelDefinition;
import com.jmpy.levels.plateform.PlateformBall;
import com.jmpy.levels.plateform.PlateformControl;
import com.jmpy.levels.plateform.PlateformView;

public class PlateformDoubleJump0View extends PlateformView {

	public PlateformDoubleJump0View(MainActivity mainActivity,
			LevelDefinition levelDefinition) {
		super(mainActivity, levelDefinition);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void init() {
		// TODO Auto-generated method stub
		super.init(new PlateformBall(this, new Point(26, 578)), 
				new PlateformDoubleJump0Land(this));
	}

}
