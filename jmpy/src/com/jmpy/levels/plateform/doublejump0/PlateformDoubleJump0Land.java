package com.jmpy.levels.plateform.doublejump0;

import android.graphics.Canvas;
import android.graphics.Path.Direction;
import android.graphics.RectF;

import com.jmpy.engine.component.LevelView;
import com.jmpy.levels.plateform.PlateformLand;
import com.jmpy.levels.plateform.reusable.DeathBlink;
import com.jmpy.levels.plateform.reusable.DeathWell;

public class PlateformDoubleJump0Land extends PlateformLand {

	public PlateformDoubleJump0Land(LevelView camera) {
		super(camera);

	    hitBox.addRect(new RectF(0, -4, 22, 1015), Direction.CCW);

	    hitBox.addRect(new RectF(2163, 1, 2177, 699), Direction.CCW);

	    hitBox.addRect(new RectF(-414, 701, -395, 767), Direction.CCW);

	    hitBox.addRect(new RectF(-502, 614, -207, 626), Direction.CCW);

	    hitBox.addRect(new RectF(-535, 332, -115, 344), Direction.CCW);

	    hitBox.addRect(new RectF(-213, 459, -193, 479), Direction.CCW);

	    hitBox.addRect(new RectF(18, 4, 2235, 24), Direction.CCW);

	    //startPoint.addRect(new RectF(26, 578, 48, 604), Direction.CCW);

	    hitBox.addRect(new RectF(-614, 388, -595, 520), Direction.CCW);

	    hitBox.addRect(new RectF(16, 608, 524, 710), Direction.CCW);

	    dieHitBox.addRect(new RectF(524, 611, 1032, 713), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(632, 462, 684, 506), Direction.CCW);

	    hitBox.addRect(new RectF(1032, 609, 1402, 711), Direction.CCW);

	    hitBox.addRect(new RectF(1804, 319, 2180, 711), Direction.CCW);

	    dieHitBox.addRect(new RectF(1400, 609, 1908, 711), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(1472, 470, 1520, 514), Direction.CCW);

	    wallJumpHitBox.addRect(new RectF(1628, 346, 1680, 394), Direction.CCW);

	    flagHitbox.addRect(new RectF(2096, 288, 2116, 308), Direction.CCW);

		updateZeroG();
	}

	@Override
	public void draw(Canvas canvas, boolean showHitBox) {

		super.draw(true, canvas);
	}

}
