<?xml version="1.0"?>
<xsl:stylesheet xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="//*[local-name()='rect']"><xsl:choose>
   <xsl:when test="contains(./@style,'#000000')">hitBox</xsl:when>
   <xsl:when test="contains(./@style,'#808000')">wallJumpHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#2aff00')">flagHitbox</xsl:when>
   <xsl:when test="contains(./@style,'#ff0000')">dieHitBox</xsl:when>
   <xsl:when test="contains(./@style,'ffff00')">bounceHitBox</xsl:when>
   
</xsl:choose>.addRect(new RectF(<xsl:value-of select="round(./@x)" disable-output-escaping="yes" />, <xsl:value-of select="round(./@y)" disable-output-escaping="yes" />, <xsl:value-of select="round(./@x)+round(./@width)" disable-output-escaping="yes" />, <xsl:value-of select="round(./@y)+round(./@height)" disable-output-escaping="yes" />), Direction.CCW);</xsl:template>

<xsl:template match="//*[local-name()='path']"><xsl:choose>
   <xsl:when test="contains(./@style,'#ff0000')">dieHitBox</xsl:when>
   <xsl:when test="contains(./@style,'#808000')">wallJumpHitBox</xsl:when>
   <xsl:when test="contains(./@style,'ffff00')">bounceHitBox</xsl:when>
</xsl:choose>.addCircle(<xsl:value-of select="round(./@*[local-name()='cx'])" />,<xsl:value-of select="round(./@*[local-name()='cy'])" />, <xsl:value-of select="round(./@*[local-name()='rx'])" />, Direction.CCW);</xsl:template>

</xsl:stylesheet>